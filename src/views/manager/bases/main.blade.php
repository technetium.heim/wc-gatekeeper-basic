<!-- Layout -->
  @extends('GKView::general.bases.main')
  @section('html-class','mobile')
  @section('body-class','mobile')

<!-- Page -->
  @section('main-sky')
    <!-- header -->

    <!-- tab -->

    <!-- navbar -->
      @yield('layout-navbar','')
      @yield('page-navbar','')

    <!-- toolbar -->
  @endsection

  @section('main-ceiling')
    <div class="row">
      @if(session('status'))
        @php
          switch (session('status')) {
            case 'error':
              $type = 'danger';
              break;
            default:
              $type = 'success';
              break;
          }
        @endphp
        <div class="alert alert-{{ $type }} no-margin-bottom">
          @if(count(session('message')) > 1)
            @foreach(session('message') as $message)
              <li class="text-left">{!! $message !!}</li>
            @endforeach
          @else 
            {!! session('message')[0] !!}
          @endif
        </div>
      @endif
    </div>
  @endsection

  @section('main-column')
    <div class="row">
      @if($column_left == 1)
        <div class="col-left row">
          @yield('page-column-left','')
          @yield('layout-column-left','')
        </div>
      @endif
      @if($column_left == 1 && $column_right == 1)
        <div class="col-center col-center-2-column row">
      @elseif($column_left == 1 || $column_right == 1)
        <div class="col-center col-center-1-column row">
      @else
        <div class="col-center col-center-0-column row">
      @endif
          @yield('page-column-center','')
        </div>
      @if($column_right == 1)
        <div class="col-right row">
          @yield('page-column-right','')
          @yield('layout-column-ight','')
        </div>
      @endif
    </div>
  @endsection

  @section('main-floor')
    <!-- footer -->
  @endsection

  @section('main-ground')
    <!-- mobile bottom navbar -->
      @yield('page-bottomnav','')
      @yield('layout-bottomnav','')
  @endsection

<!-- Modal -->
  @section('modal')
    @yield('page-modal','')
  @endsection

<!-- Script -->
  @section('script-top')
    @yield('page-script-top','')
    @yield('layout-script-top','')
    <style>
      .inline {
        display: inline-block;;
      }
      .icon {
        max-width: 20px;
        max-height: 20px;
        line-height: 20px;
        text-align: center;
      }
      .icon-container {
        max-height: 35px;
        max-width: 35px;
      }
      .padding-half {
        padding:7.5px;
      }
      .wc-btn {
        display: inline-block;
        -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none;
      }
      .wc-btn-icon {
        min-width: 50px;
        min-height: 50px;
        text-align: center;
      }
      .material-icons.fa-5x {
        font-size:5em;
      }

      .line-height-30 {
        line-height:30px;
      }
      .line-height-35 {
        line-height:35px;
      }
      .line-height-20 {
        line-height:20px;
      }
      .line-height-50 {
        line-height:50px;
      }
    </style>
  @endsection

  @section('script-analytic')  
  @endsection

  @section('script-bottom')
    @yield('page-script-bottom','')
    @yield('layout-script-bottom','')
  @endsection