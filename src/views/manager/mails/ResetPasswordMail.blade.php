<style type="text/css">
#mail-container{
	width: 500px;
	border: solid 1px;
}

.mail-title{
	height: 50px;
	background-color: #7FB3D5;
	text-align: center;
}

.mail-title span{
	font-size: 1.5em;
	color: white;
}

.mail-content{
	padding: 15px;
}

#mail-container .mail-content table{
	padding: 10px;
}

#mail-container .mail-content td{
	padding: 5px;
	padding-left: 10px;
	padding-right: 10px;
}

#mail-container .mail-content td.label{
	font-weight: bold;
}

</style>

<div id="mail-container"> 
	<div class="mail-title">
		<span>
			Reset Password
		</span>
	</div>
	<div class="mail-content">
		Hi,
		<br>
		We’ve received a request to reset your password.
		<br>
		If you didn’t make the request, just ignore this message. 
		<br>
		Otherwise, you can reset your password using this link:
		<br>
		<br>
		<a href='{{ $reset }}'>
			Click to Reset Password			
		</a>
		<br>
			
	</div>
</div>