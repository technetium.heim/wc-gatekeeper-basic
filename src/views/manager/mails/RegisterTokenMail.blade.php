<style type="text/css">
	html, body {
		margin: 0;
		padding: 0;
	}
	#mail-container{
		width: 100%;
	}
	#mail-header {
		width:100%;
		padding:15px;
		background-color:#4285f4;
		color:white;
		font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; 
		font-size: 18px;
		text-align: left;
	}
	#mail-body {
		padding:15px;
		text-align: center;
	}
	#mail-highlight {
		padding:15px;
		font-size: 24px;
		font-weight: bold;
		color: black;
	}
	#mail-button {
		padding:15px;
	}
	#mail-button a {
		display: inline-block;
		background-color: #4285f4;
		color: white;
		font-size: 16px;
		line-height: 50px;
		padding-left:15px;
		padding-right:15px;
		width: 200px;
		text-decoration: none;
	}
</style>

<div id="mail-container">
	<div id="mail-header">
		<div>{{ $RegisterToken->application->app_name }}</div>
	</div>
	<div id="mail-body">
		<div id="mail-highlight">
			<span>You've been invited to join {{ $RegisterToken->application->app_name }}.</span>
		</div>
		<div id="mail-button">
			<a href="{{$RegisterLink}}" target="_blank">Join Now</a>
		</div>
	</div>
</div>