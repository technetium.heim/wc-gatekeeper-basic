<!-- Layout -->
  @extends('GKView::manager.bases.main')

  @php
    $column_left = 1; // boolean
    $column_right = 0; // boolean
    $body_bg_color = ''; // any css color code / (null)
  @endphp

  @section('layout-column-left')
        @include('GKView::manager.layouts.menu')
  @endsection

  @section('layout-column-right')
  @endsection


  @section('layout-navbar')
    @component('WCView::general.components.headers.main')
      @section('header-content')
        <div class='bg-black border-bottom'>
          <div class='row text-left line-height-50'>
            <div class='col-xs-9'>
              <a class="wc-btn wc-btn-icon" title="Show Menu" onclick="toggleColLeft();"><i class="fa fa-fw fa-lg fa-bars"></i></a>
              <span class="text-17 text-b text-white" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif">{{ config('app.name') }}</span>
            </div>
            <div class='col-xs-3'>
              <span class="pull-right padding-right">
                <form method="POST" action="{{ route('account.manager.logout') }}">
                  <button type="submit" class="btn btn-link">
                      <span class="">Logout</span>
                  </button>
                </form>
              </span>
            </div>
          </div>
        </div>
      @endsection
    @endcomponent
  @endsection

  @section('layout-bottomnav')
  @endsection 

  @section('layout-script-bottom')
    <script>
      function toggleColLeft() {
        $('.col-left').toggle();
      }
    </script>
  @endsection 