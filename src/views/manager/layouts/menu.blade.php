@extends('GKView::manager.links.main')

{{--
::::::::::::: GUIDE :::::::::::::::
*This File is for seeting up Links list for Menu(side bar).
*Data here are feed into ('manager.links.main') for Viewing.
*('manager.links.main') are not recommended to be edit. 


Data Guide:
- $link is 1st tier list.
- $sub is the dropdown list for $link.

Options:
- [action] > actions type when clicked
    - uri : redirect to link when clicked
    - modal : open modal when clicked
    - script : onclick script when clicked
    - dropdown : open dropdown when clicked (not applicable for sub)

- [icon] > as icon shown
- [text] > as name shown
- [target] > (url) route / link for redirect, (modal) data-target for modal, (script) script for onclick, (dropdown - not applicable)
- [route] > url end for compare as selected page

Options related $sub only:
$link->ref_id > element id for sub group (IMPORTANT, to controll colapse)
$link->subs = array of SUB data. 
--}}

<style type="text/css">
#alter-menu{
    background-color: #116466;
}

#alter-menu a{
    color: white;
}   

#alter-menu a:hover{
    color: white;
    background-color: gray;
}   

#alter-menu a.active{
    color: white;
    background-color: #292d33;
}

#alter-menu .alter-sub a.active{
    color: white;
    background-color: #292d33;
}
</style>

@php
    {{-- ::::::::::::: CONTAINER: SIDE MENU ::::::::::::::: --}}
    $have_side_menu_bg = true;
    $side_menu_top_offset = '50px';
    $side_menu_class = 'bg-black';


    {{-- ::::::::::::: SETTINGS ::::::::::::::: --}}
    // Setup Subs Settings
    $subs_init_hide = true;
    $subs_offset = 2;

    {{-- ::::::::::::: SUB Data ::::::::::::::: --}}
    // action: uri, modal, script, dropdown 
    $sub_1 = array(
        (object) array(
            "icon" => "fa-arrow-circle-right",
            "text" => "Linked Apps",
            "action" => "uri",
            "target" => route('bread.application.index'),
            "route" => ["bread/application/index","bread/application/read"],
        ),
        (object) array(
            "icon" => "fa-arrow-circle-right",
            "text" => "Setup New App",
            "action" => "uri",
            "target" => route('account.manager.application.new.index'),
            "route" => "application/new",
        ),
    );

    $sub_2 = array(
        (object) array(
            "icon" => "fa-arrow-circle-right",
            "text" => "Users",
            "action" => "uri",
            "target" => route('manager.bread.user.index'),
            "route" => ["manager/bread/user/index","manager/bread/user/read"],
        ),
        (object) array(
            "icon" => "fa-arrow-circle-right",
            "text" => "Roles",
            "action" => "uri",
            "target" => route('manager.bread.role.index'),
            "route" => ["manager/bread/role/index", "manager/bread/role/read"],
        ),
    );

    $sub_3 = array(
        (object) array(
            "icon" => "fa-arrow-circle-right",
            "text" => "Register Tokens",
            "action" => "uri",
            "target" => route('bread.register_token.index'),
            "route" => ["bread/register_token/index", "bread/register_token/read"],
        ),
        (object) array(
            "icon" => "fa-arrow-circle-right",
            "text" => "Send Register Token",
            "action" => "uri",
            "target" => route('account.manager.token.create.index'),
            "route" => "register_token/create/index",
        ),
    );

    {{-- ::::::::::::: LINK Data ::::::::::::::: --}}
    $links = array
    (
        (object) array(
            "icon" => "fa-address-card-o",
            "text" => "Dashboard",
            "action" => "uri",
            "target" => route('account.manager.dashboard'),
            "route" => "dashboard",
            "ref_id" => "",
            "subs" => "",
        ),
        (object) array(
            "account" => "user",
            "icon" => "fa-sitemap",
            "text" => "Applications",
            "action" => "dropdown",
            "target" => '',
            "route" => "",
            "ref_id" => "sub-1",
            "subs" => $sub_1,
        ),
        (object) array(
            "account" => "user",
            "icon" => "fa-user",
            "text" => "Accounts",
            "action" => "dropdown",
            "target" => '',
            "route" => "",
            "ref_id" => "sub-2",
            "subs" => $sub_2,
        ),
        (object) array(
            "account" => "user",
            "icon" => "fa-user",
            "text" => "Registration",
            "action" => "dropdown",
            "target" => '',
            "route" => "",
            "ref_id" => "sub-3",
            "subs" => $sub_3,
        ),
    );

@endphp

