<!-- Layout -->
    @extends('GKView::manager.bases.main')

    @php
        $column_left = 0; // boolean
        $column_right = 0; // boolean
        $body_bg_color = '#636b6f'; // any css color code / (null)
    @endphp

    @section('layout-column-left')
    @endsection

    @section('layout-column-right')
    @endsection


    @section('layout-navbar')
    @endsection

    @section('layout-bottomnav')
    @endsection 

    @section('layout-script-bottom')
    @endsection 