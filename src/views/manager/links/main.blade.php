<!-- default setting-->
@php
	if(!isset($have_side_menu_bg)) { $have_side_menu_bg = true; }
	if(!isset($side_menu_top_offset)) { $side_menu_top_offset = '50px'; }
	if(!isset($side_menu_class)) { $side_menu_class = ''; }

	if(!isset($subs_init_hide)) { $subs_init_hide = true; }
	if(!isset($subs_offset)) { $subs_offset = 2; }

@endphp


@if( $have_side_menu_bg )
<div id="alter-menu">
<div id="sidemenu" class='hidden-xs {{ $side_menu_class }}' style='height:calc(100vh - {{ $side_menu_top_offset }})'>
@endif
	<div class="list-group">
		@foreach($links as $link)
			<!-- link default -->
				@php
					if(!isset($link->type)) { $link->type = ''; }
					if(!isset($link->account)) { $link->account = ''; }
					if(!isset($link->icon)) { $link->icon = ''; }
					if(!isset($link->text)) { $link->text = ''; }
					if(!isset($link->action)) { $link->action = ''; }
					if(!isset($link->target)) { $link->target = ''; }
					if(!isset($link->route)) { $link->route = ''; }
					if(!isset($link->ref_id)) { $link->ref_id = ''; }
					if(!isset($link->subs)) { $link->subs = []; }
				@endphp

			<!-- subs_init_state -->
				@php
					if( $subs_init_hide ) {
						$subs_init_class = "collapse"; // close
					}else{
						$subs_init_class = "collapse in"; // open
					};	
				@endphp

			<!-- active -->
				@php
					if(isset($link->route) && $link->route != '') {
						if(is_array($link->route)){
							$active = '';
							foreach( $link->route as $r){
								if (Request::is($r.'/*') || Request::is('pages/'.$r.'/*') || Request::is($r) || Request::is('pages/'.$r)) {
									$active = 'active';
								}
							}
						}else{
							if (Request::is($link->route.'/*') || Request::is('pages/'.$link->route.'/*') || Request::is($link->route) || Request::is('pages/'.$link->route)) {
								$active = 'active';
							}
							else {
								$active = '';
							}
						}
					}
					else {
						$active = '';
					}
				@endphp

			<!-- check sub if selected for collapse option -->
				@if (!empty($link->subs) ) 
					@php
						$have_selected = false; 
					@endphp
					@foreach($link->subs as $sub)
						@php
							if(isset($sub->route) && $sub->route != '') {
								if(is_array($sub->route)){
									foreach( $sub->route as $r){
										if (Request::is($r.'/*') || Request::is('pages/'.$r.'/*') || Request::is($r) || Request::is('pages/'.$r)) {
											$have_selected = 'active';
										}
									}
								}else{

									if (Request::is($sub->route.'/*') || Request::is('pages/'.$sub->route.'/*') || Request::is($sub->route) || Request::is('pages/'.$sub->route)) {
										$have_selected = true; 
									}
								}
							}
						@endphp
					@endforeach
					@php
						if($have_selected == true){
							$subs_init_class = "collapse in";
							$active = 'active';
						}
					@endphp
				@endif



			<!-- account -->
				@if(isset($link->account))
					@if($link->account == 'guest')
		            	@php $print = Auth::guest()?  true:false;  @endphp
					@elseif($link->account == 'user')
						@php $print = Auth::check()?  true:false;  @endphp
					@elseif($link->account != '')
						@php $print = Auth::guard('$link->account')->check()?  true:false;  @endphp
					@else
			            @php $print = true; @endphp
					@endif
				@else
		            @php $print = true; @endphp
				@endif

			<!-- type -->
				@if($print == true && $link->type == 'section')
		            <hr>
		            @php $print = false; @endphp
		        @endif

	        <!-- print -->
				@if($print == true)
					@if($link->action == 'uri')
						<a href="{{ $link->target }}" class="list-group-item {{ $active}}">
					@elseif($link->action == 'modal')
						<a class="list-group-item {{ $active}}" data-toggle="modal" data-dismiss="modal" data-target="{{ $link->target }}">
					@elseif($link->action == 'script')
						<a class="list-group-item {{ $active}}" onclick="{{ $link->target }}">
					@elseif($link->action == 'dropdown')
						<a class="list-group-item {{ $active}}" data-toggle="collapse" data-target="#{{ $link->ref_id }}" onclick="$('.collapse.in').collapse('hide')">
					@else
						<a class="list-group-item ">
					@endif
						@if(isset($link->icon) && $link->icon != '')
							<span class="icon">
								<i class="fa fa-fw fa-lg {{ $link->icon }}"></i>
								<i class="fa fa-fw"></i>
							</span>
						@endif
						<span>{{ $link->text }}</span>
						@if ($link->action == 'dropdown') 
							<span class="icon pull-right">
								<i class="fa fa-fw fa-sort-desc"></i>
							</span>
						@endif
					</a>
					@if (!empty($link->subs) ) 
	                    <div id="{{ $link->ref_id }}" class="list-group no-margin {{ $subs_init_class }} alter-sub">
	                        @foreach($link->subs as $submenu)

	                        	@php
		                        	if(!isset($submenu->icon)) { $submenu->icon = ''; }
									if(!isset($submenu->text)) { $submenu->text = ''; }
									if(!isset($submenu->action)) { $submenu->action = ''; }
									if(!isset($submenu->target)) { $submenu->target = ''; }
									if(!isset($submenu->route)) { $submenu->route = ''; }
								@endphp

								@php
									if(isset($submenu->route) && $submenu->route != '') {
										$active = '';
										if(is_array($submenu->route)){
											foreach( $submenu->route as $r){
												if (Request::is($r.'/*') || Request::is('pages/'.$r.'/*') || Request::is($r) || Request::is('pages/'.$r)) {
													$active = 'active';
												}
											}
										}else{

											if (Request::is($submenu->route.'/*') || Request::is('pages/'.$submenu->route.'/*') || Request::is($submenu->route) || Request::is('pages/'.$submenu->route)) {
												$active = 'active';
											}
											else {
												$active = '';
											}
										}
									}
									else {
										$active = '';
									}
								@endphp

	                     
	                            @if($submenu->action == 'uri')
									<a href="{{ $submenu->target }}" class="list-group-item {{ $active}}">
								@elseif($submenu->action == 'modal')
									<a class="list-group-item {{ $active}}" data-toggle="modal" data-dismiss="modal" data-target="{{ $submenu->target }}">
								@elseif($submenu->action == 'script')
									<a class="list-group-item {{ $active}}" onclick="{{ $submenu->target }}">
								@else
									<a class="list-group-item">
								@endif
									<div class="col-xs-offset-{{ $subs_offset }}">
	                                	@if(isset($submenu->icon) && $submenu->icon != '')
	                                		<span class="icon">
	                                    		<i class="fa fa-fw {{ $submenu->icon }} "></i>
	                                    	</span>
	                                    @endif
	                                     &nbsp;&nbsp;
	                                    <span>{{ $submenu->text }}</span>
	                                </div>
	                            </a>
	                        @endforeach
	                    </div>
	                @endif
				@endif
		@endforeach
	</div>
@if( $have_side_menu_bg )
</div>
</div>
@endif