<!-- layout -->
	@extends('GKView::manager.layouts.panel')

<!-- navbar -->
	@section('page-navbar')
	@endsection

<!-- column left -->
	@section('page-column-left')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.main')
			@slot('content_height','calc(100vh - 50px)')
			@slot('content_padding','true')
			@slot('content')
				<div class="alert alert-info">
					<i class="fa fa-lg fa-info-circle"></i>&nbsp;Send Register Token to given email.
				</div>
				@component('WCView::general.components.panels.main')
				    @slot('title')
				    	Send Register Token
					@endslot
				    @slot('content')
				    	@if ($errors->count() > 0)
							<div class="text-left alert alert-danger">
								<span class="help-block">
									<strong>{{ $errors->first() }}</strong>
								</span>
							</div>
						@endif

				    	@if ( Session::has('onsuccess') )
						    <div class="alert alert-success">
						        <strong>{{ Session::get('onsuccess') }}</strong>
						    </div>
						@endif

						<form method="POST" action="{{ route('account.manager.token.send') }}">
						@component('GKView::general.library.input_basic', [
								'show_label' => true,
								'label' => 'Email',
								'name' => 'email',
								'value' => old('email'),
								'placeholder' => 'Email Address (ex: example@example.com)',
								'required' => true,
							])
						@endcomponent

						@component('GKView::general.library.input_dropdown', [
								'show_label' => true,
								'label' => 'Application',
								'name' => 'remote_app_id',
								'selected' => old('remote_app_id'),
								'options_data' => $apps,
								'required' => true,
							])
							@slot('info')
								<a class="padding-left" href="{{ route('account.manager.application.new.index') }}">Link New App</a>
							@endslot
						@endcomponent

						<button class="btn btn-primary pull-right margin-top">
								Send Email
						</button>
						</form>
					@endslot
				@endcomponent  
			@endslot
		@endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- bottomnav -->
	@section('page-bottomnav')
	@endsection	

<!-- modal -->
	@section('page-modal')	
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection
        