@extends('WCView::general.layouts.admin.desktop.auth')

@section('content')
	<!-- BOC: form -->
		<form id="form" method="POST" action="{{route('account.manager.login')}}">
			{{ csrf_field() }}
			<div class="padding-h">
				<div class='rounded-xs bg-white max-width-xs padding margin-bottom'>
					<div>
						<!-- BOC: subtitle -->
							<div class="text-uppercase text-18 text-bold text-black line-height-50 margin-bottom">Log In</div>
						<!-- EOC -->
						<!-- BOC: alert -->
							@if ($errors->count() > 0)
								<div class="text-left alert alert-danger">
									<span class="help-block">
										<strong>{{ $errors->first() }}</strong>
									</span>
								</div>
							@endif
						<!-- EOC -->
						<!-- BOC: input -->
							<input 
								class='form-control margin-bottom line-height-50 height-50' 
								type='text'
								placeholder='Email Address' 
								name='email'
								value="{{ old('email') }}"
								required
							/>
						<!-- EOC -->
						<!-- BOC: input -->
							<input 
								class='form-control margin-bottom line-height-50 height-50' 
								type='password'
								placeholder='Password' 
								name='password'
								required
							/>
						<!-- EOC -->
						<!-- BOC: button -->
							<a 
								class='btn btn-block btn-primary margin-bottom line-height-50 height-50' 
								onclick="$('#form').submit()"
							>Log In</a>
						<!-- EOC -->
					</div>
				</div>	
			</div>
		</form>
	<!-- EOC -->
@endsection