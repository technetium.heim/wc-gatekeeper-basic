<!-- body -->
	@section('body-class','bg-blue')

<!-- layout -->
	@extends('GKView::manager.layouts.auth')

<!-- navbar -->
	@section('page-navbar')
	@endsection

<!-- column left -->
	@section('page-column-left')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.main')
			@slot('content_height','calc(100vh)')
			@slot('content_padding','true')
			@slot('content_class','text-center')
			@slot('content')
				<div class='margin-bottom text-white text-lg'>
					<span><i class='fa fa-fw fa-shield fa-lg'></i>  {{ config('app.name') }}</span>
				</div>
				<div class='rounded-xs bg-white max-width-xs padding margin-bottom'>
					<div>
						<div class="alert alert-danger">
							Your Account have insufficient permissions.
						</div>
						<div class="margin-bottom text-b" >
							{{ Auth::user()->email }}
						</div>
						<div class="margin-top">
							<form method="POST" action="{!! route('account.manager.logout') !!}">
			                  <button class="btn btn-primary btn-block" >
			                      <span>Logout & Switch Account</span>
			                  </button>
			                </form>
						</div>
						<div class="margin-top">
							<a class="btn btn-link" href="{{ route('account.user.dashboard') }}">
								Back to User Profile
							</a>
						</div>
						<div class="text-sm">
							Please Contact Admin for further information.
						</div>
					</div>
				</div>	
			@endslot
		@endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- bottomnav -->
	@section('page-bottomnav')
	@endsection	

<!-- modal -->
	@section('page-modal')
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection