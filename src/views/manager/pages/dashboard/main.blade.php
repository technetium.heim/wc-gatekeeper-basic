<!-- layout -->
	@extends('GKView::manager.layouts.panel')

<!-- navbar -->
	@section('page-navbar')
	@endsection

<!-- column left -->
	@section('page-column-left')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.main')
			@slot('content_height','calc(100vh - 50px)')
			@slot('content_padding','true')
			@slot('content')
				@component('WCView::general.components.panels.main')
				    @slot('content')
				    	<span class="text-lg text-b text-u">
				    		Account Manager Dashboard
						</span>
						<div>
							Total Application Linked: {{ $application_count }}
						</div>
						<div>
							Total User:  {{ $user_count }}
						</div>
					@endslot
				@endcomponent
			@endslot
		@endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- bottomnav -->
	@section('page-bottomnav')
	@endsection	

<!-- modal -->
	@section('page-modal')	
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection
        