<!-- layout -->
	@extends('GKView::manager.layouts.panel')

<!-- navbar -->
	@section('page-navbar')
	@endsection

<!-- column left -->
	@section('page-column-left')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.main')
			@slot('content_height','calc(100vh - 50px)')
			@slot('content_padding','true')
			@slot('content')
				@include('BreadView::main')
			@endslot
		@endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- bottomnav -->
	@section('page-bottomnav')
	@endsection	

<!-- modal -->
	@section('page-modal')	
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection
        