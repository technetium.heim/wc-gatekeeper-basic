<!-- layout -->
	@extends('GKView::manager.layouts.panel')

<!-- navbar -->
	@section('page-navbar')
	@endsection

<!-- column left -->
	@section('page-column-left')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.main')
			@slot('content_height','calc(100vh - 50px)')
			@slot('content_padding','true')
			@slot('content')
				<div class="alert alert-info">
					<i class="fa fa-lg fa-info-circle"></i>&nbsp;Setup and Linking an Application.
				</div>
				@component('WCView::general.components.panels.main')
				    @slot('title')
				    	Setup New Application
					@endslot
				    @slot('content')
					    @if ( Session::has('onsuccess') )
						    <div class="alert alert-success">
						        <strong>{{ Session::get('onsuccess') }}</strong>
						    </div>
						@endif
				    	<form method="POST" action="{{ route('account.manager.application.new') }}">
					    	@component('GKView::general.library.input_basic', [
								'show_label' => true,
								'label' => 'Application Name',
								'name' => 'app_name',
								'value' => old('app_name'),
								'placeholder' => 'Application Name',
								'required' => true,

							])
							@endcomponent
							
							@component('GKView::general.library.input_basic', [
								'show_label' => true,
								'label' => 'Url',
								'name' => 'remote_url',
								'value' => old('remote_url'),
								'placeholder' => 'Application Base Url',
								'required' => true,
							])
							@endcomponent
							
							@component('GKView::general.library.input_dropdown', [
								'show_label' => true,
								'label' => 'Role Key (filter)',
								'name' => 'role_key',
								'selected' => old('role_key'),
								'options_data' => $roles,
								'required' => true,
							])
							@slot('info')
								<a class="padding-left" href="{{ route('account.manager.application.role.index') }}">Create a new Role</a>
							@endslot
							@endcomponent

							@component('GKView::general.library.input_boolean', [
								'toggle' => true,
								'show_label' => true,
								'name' => 'public',
								'required' => true,
								'default' => 0,
								'selected' => old('public'),
								'true_display' => 'True',
								'false_display' => 'False',
							])
							@endcomponent

							<button class="btn btn-primary pull-right margin-top">
								Save
							</button>
						</form>
					@endslot
				@endcomponent  
			@endslot
		@endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- bottomnav -->
	@section('page-bottomnav')
	@endsection	

<!-- modal -->
	@section('page-modal')	
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection
        