<!-- layout -->
	@extends('GKView::manager.layouts.panel')

<!-- navbar -->
	@section('page-navbar')
	@endsection

<!-- column left -->
	@section('page-column-left')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.main')
			@slot('content_height','calc(100vh - 50px)')
			@slot('content_padding','true')
			@slot('content')
				@component('WCView::general.components.panels.main')
				    @slot('title')
				    	Create New Role
				    	<div class="pull-right">
				    		<a href="{{ route('account.manager.application.new.index') }}">
								Back to Setup New App
							</a>
				    	</div>
					@endslot
				    @slot('content')
					    @if ( Session::has('onsuccess') )
						    <div class="alert alert-success">
						        <strong>{{ Session::get('onsuccess') }}</strong>
						    </div>
						@endif
				    	<form method="POST" action="{{ route('account.manager.application.role.create') }}">

					    	@component('GKView::general.library.input_basic', [
								'show_label' => true,
								'label' => 'Name',
								'name' => 'name',
								'value' => old('name'),
								'placeholder' => 'Role Name (ex: tester)',
								'required' => true,
							])
							@endcomponent
							
							@component('GKView::general.library.input_basic', [
								'show_label' => true,
								'label' => 'Display Name',
								'name' => 'display_name',
								'value' => old('display_name'),
								'placeholder' => 'Display Name (ex: Tester)',
								'required' => true,
							])
							@endcomponent
				
							@component('GKView::general.library.input_basic', [
								'type'=> 'textarea',
								'show_label' => true,
								'label' => 'Description',
								'name' => 'description',
								'value' => old('description'),
								'placeholder' => 'Description',
								'required' => false,
							])
							@endcomponent
						

							<button class="btn btn-primary pull-right margin-top">
								Create
							</button>
						</form>
					@endslot
				@endcomponent  
			@endslot
		@endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- bottomnav -->
	@section('page-bottomnav')
	@endsection	

<!-- modal -->
	@section('page-modal')	
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection
        