@extends('WCView::general.components.navbars.main')

@php
	if(!isset($color)) { $color = 'primary'; }
	if(!isset($type)) { $type = 21; }
	if(!isset($center_align)) { $center_align = 'center'; }
@endphp