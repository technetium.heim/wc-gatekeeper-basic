<div id="modal-small-loading" class="modal add-backdrop">
    <div class="modal-dialog">
        <div class="modal-small-content">
            <div id="modal-loading">
                <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><br/><br/>
                <i>Loading...</i>
            </div>
        </div>
    </div>
</div>