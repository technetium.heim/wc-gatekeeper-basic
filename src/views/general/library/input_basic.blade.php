@php
	// default value
	if( !isset($type) ) $type = "text";
	if( !isset($id) ) $id = "";
	if( !isset($name) ) $name = "";
	if( !isset($value) ) $value = "";
	if( !isset($placeholder) ) $placeholder = "";
	if( !isset($show_label) ) $show_label = false;
	if( !isset($label) ) $label = $name;
	if( !isset($required) ) $required = false;
	if( !isset($locked) ) $locked = false;
	if( !isset($autocomplete) ) $autocomplete = false;
	if( !isset($input_class) ) $input_class = "";			
	if( !isset($show_error) ) $show_error = true;
	if( !isset($rows) ) $rows = '4';				
	if( !isset($info) ) $info = "";

	$this_errors = [];
	// error (only for important)
	if( $name == null ) array_push($this_errors , "No Input Name (name)" );
	
	// Process Data
	$required = $required? "required": "";
	$locked = $locked? "locked": "";
	$autocomplete = $autocomplete? "autocomplete": "";
	$id = ($id != null ) ? "id=".$id: ""; 

	// Process Structure
	$label_class = ( $show_label ) ? "col-xs-12 col-sm-3 text-left": ""; 
	$input_container_class = ( $show_label ) ? "col-xs-12 col-sm-9": ""; 

@endphp
<div class="row margin-bottom">
@if( $show_label )
	<label class="{{ $label_class }}"> 
	    {{ ucwords($label) }} 
		@if ( $required == 'required'  )
		 *
		@endif
	</label>
@endif 
	<div class="{{ $input_container_class }}"> 
		@if($type == 'textarea')
			<textarea class="form-control {{ $input_class }}" name="{{ $name }}" rows="{{ $rows }}" {{ $autocomplete }} {{ $required }} {{ $locked }} 
			@if($placeholder != null)
				placeholder="{{ $placeholder }}" 
			@endif
			>{{ $value }}</textarea>
		@else
			<input {{ $id }} class="form-control {{ $input_class }}" type="{{ $type }}" name="{{ $name }}" value="{{ $value }}" 
			@if($placeholder != null)
				placeholder="{{ $placeholder }}" 
			@endif
			{{ $required }} {{ $locked }} {{ $autocomplete }}>
		@endif

		@if (!empty($this_errors) && $show_error)
			Input Setup Error: 
			@foreach( $this_errors as $error)
				<span class="text-i">
					{{ $error }}	
				</span>.
			@endforeach
		@endif
		{{ $info }}
		@if ($errors->has($name))
	    <span class="help-block">
	        <strong>{{ $errors->first($name) }}</strong>
	    </span>
		@endif
	</div>
</div>

{{--
// Usage //

@component('general.library.input_basic', [
	'show_label' => true/false*,
	'input_class' => 'class_name',
	'type' => 'text*/textarea',
	'label' => 'label_name',
	'placeholder' => 'placeholder', 
	'id' => 'id',
	'name' => 'input_name',
	'value' => 'value',
	'required' => true/false*,
	'locked' => true/false*,
	'autocomplete' => true/false*,
	'show_error' => true*/false,
	'rows' => '4* : For textarea only', 
])
@slot('info')
	
@endslot
@endcomponent
--}}