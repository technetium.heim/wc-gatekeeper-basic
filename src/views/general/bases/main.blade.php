<!-- Base > Layout > View -->
<!-- This is base, whole website using the same base -->

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" class="@yield('html-class','')">
  <head>
    <title>{{ config('app.name') }} {{ config('app.stage') }}</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="title" content="{{ config('app.name') }} - {{ $title }}">
    <meta name="description" content="{{ $meta['description'] }}">
    <meta name="keywords" content="{{ config('app.name') }}">
    <meta name="author" content="{{ config('app.author') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"> 

    <!-- Private Styles -->
    <link href="{{ Cradle::assetPath('css/app.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/wcfont.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/wc.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/jquery.bootgrid.min.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/bootstrap-toggle.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    
    <!-- Scripts -->
    <script>
      window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
      ]) !!};
    </script>

    <script src="{{ Cradle::assetPath('js/app.js') }}"></script>  
    <script src="{{ Cradle::assetPath('js/moment-with-locales.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/jquery.bootgrid.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/jquery.bootgrid.fa.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/bootstrap-select.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/wc.js') }}"></script>

    @yield('script-top','')    
    @yield('script-analytic','')  
  </head>
  <body style="background-color:{{ $body_bg_color }};" class="@yield('body-class','')">
    <div id="background">
      <div id="background-overlay"></div>
    </div>
    <div class="alert" hidden>
      @yield('alert','')
    </div>

    @if(isset($errors) && $errors->any())
      <div class="pop pop-hint" onclick="$(this).hide();" hidden>
        <div>{{ $errors->first() }}</div>
      </div>
    @endif

    <div id="hint" class="pop pop-hint" onclick="$(this).hide();" hidden>
    </div>

    <div class="container-fluid">
      <!-- Placeholder for Page Design-->
      <div class="main-wrapper">
        <div class="main-sky">@yield('main-sky','')</div>
        <div class="main-ceiling">@yield('main-ceiling','')</div>
        <div class="main-column">@yield('main-column','')</div>
        <div class="main-floor">@yield('main-floor','')</div>
        <div class="main-ground">@yield('main-ground','')</div>
      </div>
      <!-- Placeholder for modal include -->
    </div>
    <div class="modal-container">
      @yield('modal','')
      @stack('addmodals')  
    </div>  
  </body>
</html>

@yield('script-bottom','')
@stack('addscripts')
<script> 
  $(document).ready(function() {
    @if(session('hint'))
      showHint("{{ session('hint') }}");
    @endif      
  });
</script>

@if(session('open-modal'))
  <script> 
    $('{{ session('open-modal') }}').modal('show');
  </script>
@endif

