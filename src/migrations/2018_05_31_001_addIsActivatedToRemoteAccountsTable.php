<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class addIsActivatedToRemoteAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remote_accounts', function (Blueprint $table) {
            $table->boolean('isActivated')->default(1)->after('remote_session_token');    
            
        });
        
        Schema::table('remote_accounts', function (Blueprint $table) {
            $table->boolean('isActivated')->default(0)->change();    
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('remote_accounts', function (Blueprint $table) {
            $table->dropColumn('isActivated');
        });
    }
}
