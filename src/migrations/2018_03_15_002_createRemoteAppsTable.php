<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemoteAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remote_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_name');
            $table->string('remote_url');
            $table->string('role_key');
            $table->boolean('public')->default(0);
            $table->boolean('pending')->default(0);
            $table->boolean('blocked')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remote_apps');
    }
}
