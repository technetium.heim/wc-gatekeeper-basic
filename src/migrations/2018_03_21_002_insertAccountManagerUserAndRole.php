<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class insertAccountManagerUserAndRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = new User;
        $user->name = 'Account Manager';
        $user->email = 'manager@gmail.com';
        $user->password = Hash::make('manager');
        $user->save();

        $role = $user->roles()->getModel();
        $role->name = "manager";
        $role->display_name = "Account Manager";
        $role->description = "Default login for account manager";
        $role->save();

        $user->attachRole($role);
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $user = User::where('email', 'manager@gmail.com')->first();
        
        $role = $user->roles()->getModel();
        $role= $role->where('name', 'manager')->first();
        $role->forceDelete();
        
        $user->forceDelete();
    }
}
