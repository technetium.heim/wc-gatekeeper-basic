<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class addAccountIdRegisterTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_tokens', function (Blueprint $table) {
            $table->integer('remote_account_id')->nullable()->after('remote_app_id');    
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('register_tokens', function (Blueprint $table) {
            $table->dropColumn('remote_account_id');
        });
    }
}
