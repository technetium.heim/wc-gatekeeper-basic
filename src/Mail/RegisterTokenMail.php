<?php

namespace Gatekeeper\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Gatekeeper\Models\RegisterToken;

class RegisterTokenMail extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct(RegisterToken $register_token, $register_link ="" )
  {
      $this->RegisterToken = $register_token;
      $this->RegisterLink = $register_link;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->from(config('mail.from.address'),config('mail.from.name'))
      ->subject( $this->RegisterToken->application->app_name. " Invitation")
      ->view('GKView::manager.mails.RegisterTokenMail', ['RegisterToken' => $this->RegisterToken, 'RegisterLink' => $this->RegisterLink]);
  }
}
