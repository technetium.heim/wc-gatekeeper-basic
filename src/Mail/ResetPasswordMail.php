<?php

namespace Gatekeeper\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Gatekeeper\Models\RegisterToken;

class ResetPasswordMail extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct( $reset_link)
  {
    $this->reset_link = $reset_link;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->from(config('mail.from.address'),config('mail.from.name'))
      ->subject( "Reset Password")
      ->view('GKView::manager.mails.ResetPasswordMail', ['reset' => $this->reset_link ]);
  }
}
