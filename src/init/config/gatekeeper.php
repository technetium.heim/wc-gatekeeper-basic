<?php

return [
    'remote_session' => [
        'lifetime' => 60*60 ,
    ],
    'register_token' => [
        'lifetime' => 60*60 ,
    ],
    'authenticated_redirect' => 'account.manager.dashboard',
    'unauthenticated_redirect' => 'account.manager.landing',
];
