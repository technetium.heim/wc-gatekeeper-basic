<?php 
Route::prefix('api')->group(function(){

Route::group(['middleware' => ['auth:api']], function () {
	Route::post('/applink','Gatekeeper\Controllers\APIGateKeeper\APIApplicationController@applink');

	Route::group(['middleware' => ['applink']], function () {

		// Admin Permission API (Required gate token)
		Route::group(['middleware' => ['gate-admin']], function () {

			// Managing User
			Route::post('/UserBrowse','Gatekeeper\Controllers\APIGateAdmin\APIRemoteAccountController@browse');
			Route::post('/UserRead','Gatekeeper\Controllers\APIGateAdmin\APIRemoteAccountController@read');
			Route::post('/UserPasswordSet','Gatekeeper\Controllers\APIGateAdmin\APIRemoteAccountController@setPassword');
			Route::post('/UserRemoteAccountCreate','Gatekeeper\Controllers\APIGateAdmin\APIRemoteAccountController@create');
			Route::post('/UserRemoteAccountLink','Gatekeeper\Controllers\APIGateAdmin\APIRemoteAccountController@link');

			// Remote Apps
			Route::post('/RemoteAppBrowse','Gatekeeper\Controllers\APIGateAdmin\APIRemoteAppController@browse');
		});

		// Auth API Path
		Route::post('/register','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccRegisterController@register');
		Route::post('/registerPrivate','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccRegisterPrivateController@register');
		Route::post('/registerVerify','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccRegisterController@verifyRegister');
		Route::post('/registerPrivateVerify','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccRegisterPrivateController@verifyRegister');
		
		Route::post('/activate','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccActivateController@activate');
		Route::post('/activatePrivate','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccActivatePrivateController@activate');
		Route::post('/login','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccLoginController@verifyLogin');
		Route::post('/verifyUser','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccLoginController@verifyUser');
		
		Route::post('/ResetPasswordEmail','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccResetPasswordEmailController@sendResetPasswordEmail');
		Route::post('/ResetPassword','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccResetPasswordController@reset');
		Route::post('/PasswordChange','Gatekeeper\Controllers\APIGateKeeper\APIRemoteAccChangePasswordController@changePassword');

		// User API Path
		Route::post('/browseUserByAppRole','Gatekeeper\Controllers\APIUser\APIUserController@browseUserByAppRole');
		Route::post('/readUserByAppRole','Gatekeeper\Controllers\APIUser\APIUserController@readUserByAppRole');
		Route::post('/readUser','Gatekeeper\Controllers\APIUser\APIUserController@readUser');
		Route::post('/readUserByRemoteAccountId','Gatekeeper\Controllers\APIUser\APIUserController@readUserByRemoteAccountId');

		// Register Token API Path
		Route::post('/browseRegisterTokenByApp','Gatekeeper\Controllers\APIRegisterToken\APIRegisterTokenController@browseRegisterTokenByApp');
		Route::post('/sendRegisterToken','Gatekeeper\Controllers\APIRegisterToken\APIRegisterTokenController@sendRegisterToken');
		Route::post('/sendWithAccRegisterToken','Gatekeeper\Controllers\APIRegisterToken\APIRegisterTokenController@sendWithAccRegisterToken');
		Route::post('/createRegisterLink','Gatekeeper\Controllers\APIRegisterToken\APIRegisterTokenController@createRegisterLink');
		Route::post('/RegisterTokenResend','Gatekeeper\Controllers\APIRegisterToken\APIRegisterTokenController@resendRegisterTokenEmail');
		// Manage Check API Path (send from Manage)
		Route::post('/verifyUserFromManage','Gatekeeper\Controllers\APIManage\APIManageCheckController@verifyUserFromManage');
	});
});

});