<?php

namespace Gatekeeper\Models;

trait UserRemoteAccountTrait
{
   public function remote_accounts(){
        return $this->hasMany('Gatekeeper\Models\RemoteAccount');
    }
  
}
