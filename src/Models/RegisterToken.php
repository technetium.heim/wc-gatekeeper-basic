<?php

namespace Gatekeeper\Models;

use App\Models\AppCradleModel;

class RegisterToken extends AppCradleModel
{
    protected $table = 'register_tokens';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','remote_app_id','register_token','expired_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function application(){
        return $this->belongsTo('Gatekeeper\Models\Application', 'remote_app_id');
    }

}
