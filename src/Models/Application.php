<?php

namespace Gatekeeper\Models;

use App\Models\AppCradleModel;

class Application extends AppCradleModel
{
    protected $table = 'remote_apps';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['app_name','remote_url','role_key','public', 'pending' ,'blocked'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
  
}
