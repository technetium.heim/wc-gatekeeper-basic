<?php

namespace Gatekeeper\Models;

use App\Models\AppCradleModel;

class RemoteAccount extends AppCradleModel
{
    protected $table = 'remote_accounts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','remote_app_id','remote_account_token','remote_session_token', 'expired_at' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function application()
    {
        return $this->belongsTo('Gatekeeper\Models\Application', 'remote_app_id');
    }
}
