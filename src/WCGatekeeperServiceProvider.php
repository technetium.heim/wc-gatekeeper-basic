<?php

namespace Gatekeeper;

use Illuminate\Support\ServiceProvider;

class WCGatekeeperServiceProvider extends ServiceProvider
{
    // protected $defer = true;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {      
        // load Gatekeeper Views
        $this->loadViewsFrom(__DIR__.'/views', 'GKView');
        
        // load Gatekeeper Migrations
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        // load Gatekeeper Routes
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadRoutesFrom(__DIR__.'/api_routes.php');

        $this->publishes([
        __DIR__.'/init/config' => base_path('/config'),       
        ],"gatekeeper-config");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {   
        $this->app['router']->aliasMiddleware('applink', "Gatekeeper\Middleware\APIApplicationFilter");
        $this->app['router']->aliasMiddleware('gatekeeper', "Gatekeeper\Middleware\GatekeeperAuth");
        $this->app['router']->aliasMiddleware('gate-admin', "Gatekeeper\Middleware\GateAdmin");
        // bind this
        // $this->app->bind('cradle-basic',function() {
        //     return new Cradle();
        // });
    }
}

