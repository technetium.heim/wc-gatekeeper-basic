<?php

namespace Gatekeeper\Controllers\APIGateKeeper;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;

use Gatekeeper\Controllers\APIResponseTrait;
use App\Models\User;
use Gatekeeper\Models\RegisterToken;
use Validator;
use Hash;
use Carbon\Carbon;

class APIRemoteAccRegisterController extends CradleController
{
    use APIResponseTrait;

    protected $page;
    protected $redirectTo = '/';

    /**
     * Register New RemoteAuth Account
     * Use for public Auth System
     * Not required register token to register
     *
     */ 

    public function verifyRegister(Request $request){
        $request_param = $request->all();

        $validator = Validator::make( $request_param, ['email' => 'required' ] , ['email.required' => 'Email is missing.'] );

        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        if( !$request->connected_public){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "The App is Not Public." ) );
            return response()->json( $response );
        }

        $user = User::where( 'email' , $request->email )->first();

        if ( !$user ) {
            $response = $this->APIResponse( $request_param, $this->getResp("S", "User Not in Record."), ['action' => 'register' ] );
            return response()->json( $response );
        }

        if ( $user->name == "DefaultUser" ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", "" ) );
            return response()->json( $response );
        }
        
        $remote_account = $user->remote_accounts()->where('remote_app_id', $request->connected_app_id)->first();
        if( !$remote_account ){
            $response = $this->APIResponse( $request_param, $this->getResp("S", "Account Not in Record."), ['action' => 'activate' ] );
            return response()->json( $response );
        }

        if( !$remote_account->isActivated ){
            $response = $this->APIResponse( $request_param, $this->getResp("S", "Account is not Activated."), ['action' => 'activate' ] );
            return response()->json( $response );
        }

        $response = $this->APIResponse( $request_param, $this->getResp("F", "Account Already Existed. Please Proceed to Login."));
        return response()->json( $response );
    }

    public function register( Request $request){

        $request_param = $request->all();
        // return response()->json($request->all());

        $validator = $this->validateInputs( $request );
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        if($request->name == 'DefaultUser'){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "Username Not Allowed, Please Use Another." ) );
            return response()->json( $response );
        }

        // Check user available (if not, create new user)
        $user = User::where( 'email' , $request->email )->first();

        if( ! $request->connected_public){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "The App is Not Public." ) );
            return response()->json( $response );
        }


        // Check Roles Exist
        if( $user){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "The Email is taken, please try another." ) );
            return response()->json( $response );
        }
        $user = new User;
        $role = $user->roles()->getModel()->where('name', $request->connected_role_key )->first();
        if ( !$role  ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", "Unable to register, please contact Admin." ,"The Role is not Exist") );
            return response()->json( $response );     
        }

        // Create new user account

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        if( !$user->hasRole($role->name) ){
            $user->attachRole( $role );
        }
        
        $session_expired_date = Carbon::now()->addSeconds( config('AccServer.remote_session.lifetime') )->toDateTimeString();

        // Create remote account
        $remote_account = $user->remote_accounts()->getModel();
        $remote_account->user_id = $user->id;
        $remote_account->remote_app_id = $request->connected_app_id;
        $remote_account->remote_account_token = $this->generateCode(20);
        $remote_account->remote_session_token = $this->generateCode(40);
        $remote_account->expired_at = $session_expired_date;
        $remote_account->isActivated = true;
        $remote_account->save();
        
        // Prepare Success return Parameter (user_id, guard_token*later)
        $response_param = array(
            'email' => $user->email,
            'remote_account_user_id' => $user->id,
            'remote_account_id' => $remote_account->id,
            'remote_account_token' => $remote_account->remote_account_token,
            'remote_session_token' => $remote_account->remote_session_token,
            'role_key' => $role->name,
        );

        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success"), $response_param);
        return response()->json( $response );
    }

    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'email' => 'required',
            'name' => 'required',
            'password' => 'required|min:8|confirmed',
        ];

        $message = [
            'email.required' => 'Email is missing.',
            'name.required' => 'Name is missing.',
            'password.required' => 'Password is missing.',
        ];

        $validator = Validator::make( $request->all() , $rules, $message );

        return $validator;
    }

}
