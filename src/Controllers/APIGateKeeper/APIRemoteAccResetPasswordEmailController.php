<?php

namespace Gatekeeper\Controllers\APIGateKeeper;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;

use Gatekeeper\Controllers\APIResponseTrait;
use App\Models\User;
use Validator;

use Cradle\basic\foundation\auth\CradleSendsPasswordResetEmails;
use Gatekeeper\Mail\ResetPasswordMail;
use Cradle\tools\WCMail;

class APIRemoteAccResetPasswordEmailController extends CradleController
{
    use APIResponseTrait;
    use CradleSendsPasswordResetEmails;

    protected $page;
    protected $redirectTo = '/';

    /**
     * Register New RemoteAuth Account
     *
     * @var string
     */    
    public function sendResetPasswordEmail(Request $request){
        
        $request_param = $request->all();

        $validator = $this->validateInputs($request);
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        // Check user available 
        $user = User::where( 'email' , $request->email )->first();
       
        if(!$user){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'Email not in record' ) );
            return response()->json( $response );
        }

        $remote_account = $user->remote_accounts()->where('remote_app_id', $request->connected_app_id )->first();
        if(!$remote_account){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'Email not registered to this App' ) );
            return response()->json( $response );
        }
        
        //send reset email
        $token = $this->broker()->createToken($user);
        $reset_link = $request->reset_password_route."/".$token;
        Mail::to($request->email)->send(new ResetPasswordMail($reset_link) );

        $send = new WCMail;
        $send = $send->send( $request->email,  new ResetPasswordMail($reset_link) );

        if( $send == 'Success'){
            $email_sent = 'Yes';
            $msg = 'Success';
        }else{
            $email_sent = 'No';
            $msg = 'Email Sent Failed! ('.$send.')';
        }

        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success"));
        return response()->json( $response );
    }

    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'email' => 'required',
            'reset_password_route' => 'required',
        ];

        $message = [
            'email.required' => 'Email is missing.',
            'reset_password_route.required' => 'Reset Password Route is missing.',
        ];

        $validator = Validator::make( $request->all() , $rules, $message );

        return $validator;
    }

    // public function broker()
    // {
    //     return Password::broker();
    // }
}
