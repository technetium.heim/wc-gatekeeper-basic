<?php

namespace Gatekeeper\Controllers\APIGateKeeper;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;

use Validator;
use Gatekeeper\Controllers\APIResponseTrait;
use Gatekeeper\Models\Application;

/*
    This file is only use to add new remote_apps from AppServer.
    Require to cheange the pending status using Manager account.  
*/
class APIApplicationController extends CradleController
{
    use APIResponseTrait;

    public function applink(Request $request)
    {   
        $request_param = $request->all();

        // validate all inputs
        $validator = $this->validateInputs($request_param);
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        // check if the data is existed
        $check_role_key = Application::where( 'role_key' , $request->role_key );
        if( $check_role_key->count() > 0 ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "Role Key Existed.") );
            return response()->json( $response );   
        }

        $role_model = config('role.role');
        $role = new $role_model;
        $role = $role->where('name', $request->role_key )->first();

        if( !$role ){
            $role = new $role_model;
            $role->name = $request->role_key;
            $role->display_name = $request->app_name.' Role Key';
            $role->description = 'Application Role Key by Applink';
            $role->save(); 
        }else{
            $response = $this->APIResponse( $request_param, $this->getResp("F", "Role Key Existed.") );
            return response()->json( $response );   
        }   

        // create a pending remote apps
        $remote_app = new Application;
        $remote_app->app_name = $request->app_name;
        $remote_app->remote_url = $request->base_url;
        $remote_app->role_key = $request->role_key;
        $remote_app->public = $request->public;
        $remote_app->pending = true;
        $remote_app->save();

        $response = $this->APIResponse( $request_param, $this->getResp("S") );
        return response()->json($response);
    }

    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'app_name' => 'required|string',
            'base_url' => 'required|string',
            'role_key' => 'required|string',
            'public' => 'required|boolean',
        ];

        $message = [
            'app_name.required' => 'App Name is missing.',
            'base_url.required' => 'Base URL is missing.',
            'role_key.required' => 'Role Filter is missing.',
        ];

        $validator = Validator::make( $request, $rules, $message );

        return $validator;
    }
}
