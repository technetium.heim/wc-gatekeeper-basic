<?php

namespace Gatekeeper\Controllers\APIGateKeeper;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;

use Gatekeeper\Controllers\APIResponseTrait;
use App\Models\User;
use Gatekeeper\Models\Application;
use Validator;
use Hash;

class APIRemoteAccLoginController extends CradleController
{
    use APIResponseTrait;

    protected $page;
    protected $redirectTo = '/';

    /**
     * Register New RemoteAuth Account
     *
     * @var string
     */    
    // TO BE OBSULUTE

    ///////////////////////////////
    public function verifyUser(Request $request){
        // check if the user available in database
        $request_param = $request->all();

        $validator = Validator::make( $request_param, ['email' => 'required'] , ['email.required' => 'Email is missing.'] );
        
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }        

        // Check user available
        $user = User::where( 'email' , $request->email )->first();
        if( !$user ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'User Not In Record.' ));
            return response()->json( $response );
        }

        if( $user->name == 'DefaultUser'){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'Please Complete Registration Process.' ));
            return response()->json( $response );
        }

        $remote_accounts = $user->remote_accounts()->where('remote_app_id', $request->connected_app_id);
        if( $remote_accounts->count() < 1 ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "User Not Registered To This Application" ) );
            return response()->json( $response );
        }

        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success") );
            return response()->json( $response );
    }
    //////////////////////////////////////

    public function verifyLogin(Request $request){
        
        $request_param = $request->all();

        $validator = $this->validateInputs( $request_param );

        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        // Check user available
        $user = User::where( 'email' , $request->email )->first();
       
        if(!$user){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'User not in record' ) );
            return response()->json( $response );
        }

        if( $user->name == 'DefaultUser'){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'Please Complete Registration Process.' ));
            return response()->json( $response );
        }

        $remote_account = $user->remote_accounts()->where('remote_app_id', $request->connected_app_id )->first();
        if( !$remote_account ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "User Not Register To This Application.", "Remote Account not in record") );
            return response()->json( $response );
        }

        if( !$remote_account->isActivated ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "User Account Not Activeted.") );
            return response()->json( $response );
        }

        // check Password (validate credential)
        if( !Hash::check($request->password, $user->password) ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "Authentication Failed", "Wrong Password") );
            return response()->json( $response );      
        }

        // Generate New Session Code and save
        $remote_session_token = $this->generateCode(40);
        $remote_account->remote_session_token = $remote_session_token;
        $remote_account->save();

        $response_param = array(
            'email' => $request->email,
            'remote_account_user_id' => $user->id,
            'remote_account_id' => $remote_account->id,
            'remote_account_token' => $remote_account->remote_account_token,
            'remote_session_token' => $remote_session_token,
        );
            
        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success"), $response_param);
        return response()->json( $response );
    }

    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'email' => 'required',
            'password' => 'required',
        ];

        $message = [
            'email.required' => 'Email is missing.',
            'password.required' => 'Password is missing.',
        ];

        $validator = Validator::make( $request, $rules, $message );

        return $validator;
    }
}
