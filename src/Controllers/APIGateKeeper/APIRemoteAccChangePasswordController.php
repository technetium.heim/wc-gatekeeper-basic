<?php

namespace Gatekeeper\Controllers\APIGateKeeper;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Gatekeeper\Controllers\APIResponseTrait;
use App\Models\User;
use Validator;
use Hash;

class APIRemoteAccChangePasswordController extends CradleController
{
    use APIResponseTrait;
    protected $page;
    protected $redirectTo = '/';

    /**
     * Register New RemoteAuth Account
     *
     * @var string
     */    

    public function changePassword(Request $request)
    {
        $request_param = $request->all();
        // return response()->json($request->all());

        $validator = $this->validateInputs( $request );
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        // Check user available 
        $user = User::where( 'email' , $request->email )->first();
       
        if(!$user){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'Email not in record' ) );
            return response()->json( $response );
        }

        $remote_account = $user->remote_accounts()->where('remote_app_id', $request->connected_app_id )->first();
        if(!$remote_account){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'Email not registered to this App' ) );
            return response()->json( $response );
        }

        // match password
        if( !Hash::check($request->old_password, $user->password) ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "Authentication Failed", "Wrong Password") );
            return response()->json( $response );      
        }

        $user->password = Hash::make( $request->password);
        $user->save();

        $response = $this->APIResponse( $request_param, $this->getResp("S", "Password Updated") );
        return response()->json( $response );
    }

    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'old_password' => 'required',
            'password' => 'required|min:8|confirmed',
        ];

        $message = [
            'old_password.required' => 'Password is missing.',
            'password.required' => 'New Password is missing.',
        ];

        $validator = Validator::make( $request->all() , $rules, $message );

        return $validator;
    }
}
