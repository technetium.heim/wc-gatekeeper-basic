<?php

namespace Gatekeeper\Controllers\APIGateKeeper;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Gatekeeper\Controllers\APIResponseTrait;
use App\Models\User;
use Validator;
use Cradle\basic\foundation\auth\CradleResetsPasswords;
use Illuminate\Support\Facades\Password;

class APIRemoteAccResetPasswordController extends CradleController
{
    use APIResponseTrait;
    use CradleResetsPasswords;
    protected $page;
    protected $redirectTo = '/';

    /**
     * Register New RemoteAuth Account
     *
     * @var string
     */    

    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();
    }

    public function reset(Request $request)
    {
        $request_param = $request->all();
        // return response()->json($request->all());

        $validator = $this->validateInputs( $request );
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        // Check user available 
        $user = User::where( 'email' , $request->email )->first();
       
        if(!$user){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'Email not in record' ) );
            return response()->json( $response );
        }

        $remote_account = $user->remote_accounts()->where('remote_app_id', $request->connected_app_id )->first();
        if(!$remote_account){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'Email not registered to this App' ) );
            return response()->json( $response );
        }

        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        if($response == Password::PASSWORD_RESET ){   
            $user = User::where( 'email' , $request->email )->first();
            $remote_account = $user->remote_accounts()->where('remote_app_id', $request->connected_app_id )->first();

            $response_param = array(
                'email' => $request->email,
                'remote_account_user_id' => $user->id,
                'remote_account_id' => $remote_account->id,
                'remote_account_token' => $remote_account->remote_account_token,
                'remote_session_token' => $remote_account->remote_session_token
            );

            $response = $this->APIResponse( $request->all(), $this->getResp("S", "Success"), $response_param);
            return response()->json( $response );
        }
        
        $response = $this->APIResponse( $request->all(), $this->getResp("F", trans( $response )));
        return response()->json( $response );  
    }

    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'email' => 'required',
            'password' => 'required|min:8|confirmed',
            'token' => 'required',
        ];

        $message = [
            'email.required' => 'Email is missing.',
            'password.required' => 'Password is missing.',
            'token.required' => 'Token is missing.',
        ];

        $validator = Validator::make( $request->all() , $rules, $message );

        return $validator;
    }
}
