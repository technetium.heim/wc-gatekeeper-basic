<?php

namespace Gatekeeper\Controllers;

/* 
This File is to setup response Format for API file.
To be include and use in Controller.
*/

trait APIResponseTrait
{
    protected function getResp(  $status , $failed_reason="" , $failed_reason_dev="" ) {
        return (object)[
            "Status" => $status,
            "FailedReason" => $failed_reason,
            "FailedReasonDeveloper" => $failed_reason_dev
        ];
    }

    protected function APIResponse( $request , $resp, $result=[]) {

        return (object)[
            "Req" =>  $request,
            "Resp" => $resp,
            "Result" => $result
        ];
    }

    protected function generateCode($length, $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }

        return $str;
    }
}