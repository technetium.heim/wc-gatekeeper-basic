<?php

namespace Gatekeeper\Controllers\APIUser;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;

use Validator;
use Gatekeeper\Controllers\APIResponseTrait;
use Gatekeeper\Models\RemoteAccount;
use App\Models\User;
use Gatekeeper\Models\Application;
/*
  
*/
class APIUserController extends CradleController
{
    use APIResponseTrait;

    public function browseUserByAppRole(Request $request){

        $request = $this->processAppId($request);
        $request_param = $request->all();
             
        // validate all inputs
        $validator = $this->validateInputs($request_param);
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        //Check App ID
        $app = Application::find($request->app_id);
        if ( !$app ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", "Application Not Registered." ) );
            return response()->json( $response );
        }

        $remote_accounts = RemoteAccount::select('id','user_id')->where('remote_app_id', $request->app_id)->get();
        
        $users = [];
        foreach($remote_accounts as $remote_account){
            $user = User::find($remote_account->user_id);
            if($user){
                $user->account_id = $remote_account->id;
                $user->account_app_id = $request->app_id;
                array_push($users, $user);
            }     
        }

        $response = $this->APIResponse( $request_param, $this->getResp("S"), $users );
        return response()->json($response);
    }

    public function readUser(Request $request){

        $request_param = $request->all();
           
        $result = User::find($request->user_id);

        $response = $this->APIResponse( $request_param, $this->getResp("S"), $result );
        return response()->json($response);
    }

    public function readUserByAppRole(Request $request){
        $request = $this->processAppId($request);
        $request_param = $request->all();
        
        // validate all inputs
        $validator = $this->validateInputs($request_param);
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        $result = User::find($request->user_id);

        $remote_account = RemoteAccount::where('user_id',$request->user_id )
                                        ->where('remote_app_id', $request->app_id)
                                        ->first();
        $result->account_id = $remote_account->id;
        $result->isActivated = $remote_account->isActivated;

        $response = $this->APIResponse( $request_param, $this->getResp("S"), $result );
        return response()->json($response);
    }

    public function readUserByRemoteAccountId(Request $request){
        $request_param = $request->all();
        
        // Validation 
        $rules = [
            'remote_account_id' => 'required',
        ];

        $message = [
            'remote_account_id.required' => 'Remote Account Id is missing.',
        ];

        $validator = Validator::make( $request_param, $rules, $message );
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        $remote_account = RemoteAccount::find( $request->remote_account_id );
        $result = User::find( $remote_account->user_id);

        $result->remote_account_id = $remote_account->id;
        $result->isActivated = $remote_account->isActivated;

        $response = $this->APIResponse( $request_param, $this->getResp("S"), $result );
        return response()->json($response);
    }

    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'app_id' => 'required',
        ];

        $message = [
            'app_id.required' => 'App Id/ App Name is missing.',
        ];

        $validator = Validator::make( $request, $rules, $message );

        return $validator;
    }

    protected function processAppId( $request ){
      if( !$request->app_id && $request->app_name){
        $request->request->add(['app_id' => Application::where('app_name', $request->app_name )->first()->id ]);
      }

      return $request;
    }
}
