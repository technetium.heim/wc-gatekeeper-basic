<?php

namespace Gatekeeper\Controllers\APIManage;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;

use Gatekeeper\Controllers\APIResponseTrait;
use App\Models\User;
use Gatekeeper\Models\Application;
use Validator;
use Hash;

class APIManageCheckController extends CradleController
{
    use APIResponseTrait;

    protected $page;
    protected $redirectTo = '/';

    /**
     * Register New RemoteAuth Account
     *
     * @var string
     */    

    public function verifyUserFromManage(Request $request){
        // check if the user available in database
        $request_param = $request->all();

        $validator = $this->validateInputs( $request_param );

        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        // Check user available
        $user = User::where( 'email' , $request->email )->first();
        if( !$user ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'User not in record' ), [ 'action' => 'register'] );
            return response()->json( $response );
        }

        $remote_account = $user->remote_accounts()->where('id', $request->remote_account_id)->first();
        if( !$remote_account ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "Remote Account not in record" ) );
            return response()->json( $response );
        }

        if( $remote_account->remote_account_token != $request->remote_account_token ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", "Token Mismatch" ) );
            return response()->json( $response );
        }

        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success"), ['remote_account_id' => $remote_account->id, 'remote_account_token' => $remote_account->remote_account_token] );
            return response()->json( $response );
    }


    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'email' => 'required',
            'remote_account_id' => 'required',
            'remote_account_token' => 'required',
        ];

        $message = [
            'email.required' => 'Email is missing.',
            'remote_account_id.required' => 'Remote Account ID is missing.',
            'remote_account_token.required' => 'Remote Account Token is missing.',
        ];

        $validator = Validator::make( $request, $rules, $message );

        return $validator;
    }
}
