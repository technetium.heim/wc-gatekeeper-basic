<?php

namespace Gatekeeper\Controllers\APIGateAdmin;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;

use Gatekeeper\Controllers\APIResponseTrait;
use Gatekeeper\Models\Application;
use Validator;
use Hash;
use Carbon\Carbon;

class APIRemoteAppController extends CradleController
{
    use APIResponseTrait;

    protected $page;
    protected $redirectTo = '/';

    /**
     * Required GATE TOKEN to access method
     * browse Remote Apps
     * used by Admin User ONLY
     * Not required register token
     *
     */ 

    public function browse(Request $request){
        $request_param = $request->all();

        $apps = Application::all();

        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success"), $apps);
        return response()->json( $response );
    }
}
