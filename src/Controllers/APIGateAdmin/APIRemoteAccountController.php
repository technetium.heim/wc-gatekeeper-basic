<?php

namespace Gatekeeper\Controllers\APIGateAdmin;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;

use Gatekeeper\Controllers\APIResponseTrait;
use App\Models\User;
use Gatekeeper\Models\RegisterToken;
use Gatekeeper\Models\Application;
use Validator;
use Hash;
use Carbon\Carbon;

class APIRemoteAccountController extends CradleController
{
    use APIResponseTrait;

    protected $page;
    protected $redirectTo = '/';

    /**
     * Required GATE TOKEN to access method
     * browse User
     * used by Admin User ONLY
     * Not required register token
     *
     */ 

    public function browse(Request $request){
        $request_param = $request->all();

        $per_page = ($request->per_page)? $request->per_page : 20;
        $current_page = ($request->current_page)? $request->current_page: 1;

        $users = User::orderBy('email')->paginate($per_page, ['*'], 'page', $current_page );

        foreach( $users as $user ){
            $user->remote_accounts = $user->remote_accounts()->count();
        }

        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success"), $users);
        return response()->json( $response );
    }

    /**
     * Required GATE TOKEN to access method
     * read User
     * used by Admin User ONLY
     * Not required register token
     *
     */ 

    public function read(Request $request){
        $request_param = $request->all();
        
        //BOC: Validation 
            $rules = [
                'user_id' => 'required',
            ];

            $message = [
                'user_id.required' => 'User ID is missing.',
            ];

            $validator = Validator::make( $request->all() , $rules, $message );
            if ( $validator->fails() ) {
                $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
                return response()->json( $response );
            }
        //EOC
        //BOC: Check user available
            $user = User::find($request->user_id);
            if( !$user){
                $response = $this->APIResponse( $request_param, $this->getResp("F", "User not found." ) );
                return response()->json( $response );
            }

            $remote_accounts = [];
            $ra = $user->remote_accounts()->get();

            foreach($ra as $a){
                array_push($remote_accounts, (object)[
                    'id' => $a->id,
                    'remote_app_id' => $a->remote_app_id,
                    'remote_app_name' => Application::find($a->remote_app_id)? Application::find($a->remote_app_id)->app_name: 'Error: Remote App Not Found.',
                    'isActivated' => $a->isActivated,
                ]);    
            }

            $user->remote_accounts = $remote_accounts;
        //EOC
        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success"), $user);
        return response()->json( $response );    
    }

    /**
     * Required GATE TOKEN to access method
     * Create new user & remote account
     * used by Admin User ONLY
     * Not required register token
     *
     */ 

    public function create(Request $request){

        $request_param = $request->all();

        //BOC: Validation 
            $validator = $this->validateInputs( $request );
            if ( $validator->fails() ) {
                $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
                return response()->json( $response );
            }

            if($request->name == 'DefaultUser'){
                $response = $this->APIResponse( $request_param, $this->getResp("F", "Username Not Allowed, Please Use Another." ) );
                return response()->json( $response );
            }       
        //EOC
        //BOC: Check App available
            $app = Application::find($request->remote_app_id);

            if( !$app ){
                $response = $this->APIResponse( $request_param, $this->getResp("F", "Remote App not found." ) );
                return response()->json( $response );
            }
        //EOC
        //BOC: Check user available (if not, create new user)
            $user = User::where( 'email' , $request->email )->first();
            if( $user){
                $response = $this->APIResponse( $request_param, $this->getResp("F", "The Email is taken, please try another." ) );
                return response()->json( $response );
            }

            $user = new User;
        //EOC
        //BOC: Get Role Key For the App
            $role = $user->roles()->getModel()->where('name', $app->role_key )->first();
            if ( !$role  ) {
                $response = $this->APIResponse( $request_param, $this->getResp("F", "Role Key for Remote App not found.") );
                return response()->json( $response );     
            }
        //EOC
        //BOC: Create new user account
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();
        //EOC
        //BOC: Attach Role to User
            if( !$user->hasRole($role->name) ){
                $user->attachRole( $role );
            }
        
        //EOC
        //BOC: Create remote account
            $session_expired_date = Carbon::now()->addSeconds( config('AccServer.remote_session.lifetime') )->toDateTimeString();

            $remote_account = $user->remote_accounts()->getModel();
            $remote_account->user_id = $user->id;
            $remote_account->remote_app_id = $request->remote_app_id;
            $remote_account->remote_account_token = $this->generateCode(20);
            $remote_account->remote_session_token = $this->generateCode(40);
            $remote_account->expired_at = $session_expired_date;
            $remote_account->isActivated = true;
            $remote_account->save();
        //EOC
        //BOC: Prepare Success return Parameter (user_id, guard_token*later)
            $response_param = array(
                'email' => $user->email,
                'remote_account_user_id' => $user->id,
                'remote_account_id' => $remote_account->id,
                'remote_account_token' => $remote_account->remote_account_token,
                'remote_session_token' => $remote_account->remote_session_token,
                'role_key' => $role->name,
            );
        //EOC
        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success"), $response_param);
        return response()->json( $response );
    }

    /**
     * Required GATE TOKEN to access method
     * Link existing user to App (create remote account)
     * used by Admin User ONLY
     * Not required register token
     *
     */ 

    public function link(Request $request){
        $request_param = $request->all();
        
        //BOC: Validation 
            $rules = [
                'user_id' => 'required',
                'remote_app_id' => 'required',
            ];

            $message = [
                'user_id.required' => 'User ID is missing.',
                'remote_app_id.required' => 'Remote App ID is missing.',
            ];

            $validator = Validator::make( $request->all() , $rules, $message );
            if ( $validator->fails() ) {
                $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
                return response()->json( $response );
            }
        //EOC
        //BOC: Check App available
            $app = Application::find($request->remote_app_id);

            if( !$app ){
                $response = $this->APIResponse( $request_param, $this->getResp("F", "Remote App not found." ) );
                return response()->json( $response );
            }
        //EOC
        //BOC: Check user available
            $user = User::find($request->user_id);
            if( !$user){
                $response = $this->APIResponse( $request_param, $this->getResp("F", "The Email is not registered." ) );
                return response()->json( $response );
            }
        //EOC
        //BOC: Check user already linked
            $modal = $user->remote_accounts()->getModel();
            $remote_account = $modal->where('user_id', $request->user_id)
                                    ->where('remote_app_id', $request->remote_app_id)
                                    ->first();

            if($remote_account){
                $response = $this->APIResponse( $request_param, $this->getResp("F", "User already linked to this Remote App.") );
                return response()->json( $response );  
            }
        //EOC
        //BOC: Get Role Key For the App
            $role = $user->roles()->getModel()->where('name', $app->role_key )->first();
            if ( !$role  ) {
                $response = $this->APIResponse( $request_param, $this->getResp("F", "Role Key for Remote App not found.") );
                return response()->json( $response );     
            }
        //EOC
        //BOC: Attach Role to User
            if( !$user->hasRole($role->name) ){
                $user->attachRole( $role );
            }
        //EOC
        //BOC: Create remote account
            $session_expired_date = Carbon::now()->addSeconds( config('AccServer.remote_session.lifetime') )->toDateTimeString();

            $remote_account = $user->remote_accounts()->getModel();
            $remote_account->user_id = $user->id;
            $remote_account->remote_app_id = $request->remote_app_id;
            $remote_account->remote_account_token = $this->generateCode(20);
            $remote_account->remote_session_token = $this->generateCode(40);
            $remote_account->expired_at = $session_expired_date;
            $remote_account->isActivated = true;
            $remote_account->save();
        //EOC
        //BOC: Prepare Success return Parameter (user_id, guard_token*later)
            $response_param = array(
                'email' => $user->email,
                'remote_account_user_id' => $user->id,
                'remote_account_id' => $remote_account->id,
                'remote_account_token' => $remote_account->remote_account_token,
                'remote_session_token' => $remote_account->remote_session_token,
                'role_key' => $role->name,
            );
        //EOC
        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success"), $response_param);
        return response()->json( $response );
    }

    /**
     * Required GATE TOKEN to access method
     * Edit User (password only)
     * used by Admin User ONLY
     * Not required register token
     *
     */ 
    public function setPassword(Request $request){
        $request_param = $request->all();
        
        //BOC: Validation 
            $rules = [
                'user_id' => 'required',
                // 'email' => 'required',
                // 'name' => 'required',
                'password' => 'required|min:8',
            ];

            $message = [
                'user_id.required' => 'User ID is missing.',
                // 'email.required' => 'Email is missing.',
                // 'name.required' => 'Name is missing.',
                'password.required' => 'Password is missing.',
            ];

            $validator = Validator::make( $request->all() , $rules, $message );
            if ( $validator->fails() ) {
                $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
                return response()->json( $response );
            }
        //EOC
        //BOC: Check user available
            $user = User::find($request->user_id);
            if( !$user){
                $response = $this->APIResponse( $request_param, $this->getResp("F", "User not found." ) );
                return response()->json( $response );
            }
        //EOC
        //BOC: Set new password
            $user->password = Hash::make($request->password);
            $user->save();
        //EOC
        $response = $this->APIResponse( $request_param, $this->getResp("S", "Success"));
        return response()->json( $response );
    }

    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'email' => 'required',
            'name' => 'required',
            'password' => 'required|min:8',
            'remote_app_id' => 'required',
        ];

        $message = [
            'email.required' => 'Email is missing.',
            'name.required' => 'Name is missing.',
            'password.required' => 'Password is missing.',
            'remote_app_id.required' => 'Remote App ID is missing.',
        ];

        $validator = Validator::make( $request->all() , $rules, $message );

        return $validator;
    }

}
