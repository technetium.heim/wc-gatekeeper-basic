<?php

namespace Gatekeeper\Controllers;

/* 
This file is simple way to generate data structure required for Cradle Input library
*/

trait InputDataGeneratorTrait
{
    protected function generateDropdown(  $rows_collection , $value_col , $name_col , $type="value_name_as_key" ) {
        
        $data_array = [];
        foreach( $rows_collection  as $row ){
            $data = [
                'value' => $row->{$value_col},
                'name' => $row->{$name_col},
            ];
            array_push( $data_array, $data);
        }

        return $data_array;
    }

    protected function generateCode($length, $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }

        return $str;
    }
}