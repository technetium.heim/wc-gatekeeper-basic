<?php

namespace Gatekeeper\Controllers\AccountManager;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;

use Validator;
use Log; 
use Session;

class AccountController extends CradleController
{
  protected $page;
  protected $redirectTo = '/';

  public function browse()
  {
    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view('GKView::manager/pages/account/browse',$this->page);
  }


}
