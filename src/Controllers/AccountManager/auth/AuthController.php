<?php

namespace Gatekeeper\Controllers\AccountManager\auth;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;
use Cradle\basic\foundation\auth\CradleAuthenticatesUsers;
use GuzzleHttp\Client;
use Validator;
use Log; 
use Session;
use App\Models\User;
use Gatekeeper\Models\Application;

class AuthController extends CradleController
{
  use CradleAuthenticatesUsers;
  protected $page;
  protected $redirectTo = '/';

  public function index()
  {
    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view('GKView::manager/pages/auth/landing',$this->page);
  }

  public function dashboard(){
    $this->page['user_count'] = User::get()->count();
    $this->page['application_count'] = Application::get()->count();

    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view('GKView::manager/pages/dashboard/main', $this->page);
  }

  public function restrict(){
    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view('GKView::manager/pages/auth/restrict', $this->page);
  }

  protected function loginRules()
  {   
    return [
        $this->username() => 'required|string',
        'password' => 'required|string',
    ];
  }

  public function login(Request $request)
  {

      $v = $this->validator($request->all()); 
      if ($request->expectsJson()) {
          //Validate using ajax 
          if ($v->fails()) return response()->json($v->errors());
      }else {
          $v->validate();
      }

      $user = User::where( $this->username(), $request->{$this->username()})->first();

      if( !$user ){
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors( ['error' => 'Invalid Credential'] );
      }

      if( !$user->hasRole('manager')){
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors( ['error' => 'Insufficient Permission'] );
      }

      // If the class is using the ThrottlesLogins trait, we can automatically throttle
      // the login attempts for this application. We'll key this by the username and
      // the IP address of the client making these requests into this application.
      if ($this->hasTooManyLoginAttempts($request)) {
          $this->fireLockoutEvent($request);

          return $this->sendLockoutResponse($request);
      }

      if ($this->attemptLogin($request)) {
          return $this->sendLoginResponse($request);
      }

      // If the login attempt was unsuccessful we will increment the number of attempts
      // to login and redirect the user back to the login form. Of course, when this
      // user surpasses their maximum number of attempts they will get locked out.
      $this->incrementLoginAttempts($request);

      return $this->sendFailedLoginResponse($request);
  }

  protected function cradleRedirect($condition) {
        switch ($condition) {
            case 'login':
                return 'account.manager.dashboard'; 
                break;
            case 'logout':
                return 'account.manager.landing'; 
                break;
            default:
                return 'account.manager.dashboard';
                break;
        }    
    }

    public function username()
    {
        return 'email';
    }

}
