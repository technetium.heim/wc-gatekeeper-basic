<?php

namespace Gatekeeper\Controllers\AccountManager;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;
use Gatekeeper\Models\Application;
use Gatekeeper\Models\RegisterToken;

use Gatekeeper\Controllers\InputDataGeneratorTrait;
use Illuminate\Support\Facades\Mail;
use Gatekeeper\Mail\RegisterTokenMail;
use App\Models\User;

use Validator;
use Carbon\Carbon;
use Cradle\tools\WCMail;

class RegisterTokenController extends CradleController
{
  use InputDataGeneratorTrait;
  protected $page;
  protected $redirectTo = '/';

  public function index(Request $request)
  {
    // prepare role list 
    $apps = Application::get();
    // (remove Admin and Manager)
    
    $apps = $this->generateDropdown( $apps, 'id', 'app_name' );
    // dd($apps);
    $this->page['apps'] = $apps;
    $this->page['title'] = ucwords(config('app.name'));
    $this->page['meta']['description'] = ucwords(config('app.name'));
    return view('GKView::manager/pages/register_token/send',$this->page);
  }

  public function sendToken(Request $request)
  {
    // Set initial data for pending and blocked
    $request->request->add(['register_token' => $this->generateCode( 8) ]);

    $request_param = $request->all();

    // validate all inputs
    $validator = $this->validateInputs($request_param);
    if ( $validator->fails() ) {
        return redirect()->back()
          ->withInput($request->all())
          ->withErrors($validator->errors());
    }
    
    // Expired data 
    $expired_date = Carbon::now()->addSeconds( config('gatekeeper.remote_session.lifetime') )->toDateTimeString();
  
    $register_token = RegisterToken::where('email', $request->email )->where('remote_app_id', $request->remote_app_id )->first();

    $user = User::where('email', $request->email )->first();
    if($user){
      $remote_accounts = $user->remote_accounts();
      if( $remote_accounts->get() ){
        if( $remote_account = $remote_accounts->where('remote_app_id', $request->remote_app_id)->first() ){
          // Cannot create register token if remote_account is EXIST and IsActivated.
          if($remote_account->isActivated){
              return redirect()->back()
            ->withInput($request->all())
            ->withErrors(['error' => $request->email.' already registered to this app.']);
          }                    
        }
      }
    }

    if ($register_token){
      // replace existing data
      $register_token->register_token = $request->register_token;
      $register_token->expired_at = $expired_date;
      $register_token->save();
    }else{
      // Save new data
      $register_token = new RegisterToken;
      $register_token->email = $request->email;
      $register_token->remote_app_id = $request->remote_app_id;
      $register_token->register_token = $request->register_token;
      $register_token->expired_at = $expired_date;
      $register_token->save();
    }
    
    // return redirect()->back()->with('onsuccess','Register Token Created! (send email is not working)');
    // check for email settings
    // Mail::to($request->email)->send(new RegisterTokenMail($register_token) );

    // Mail::to($request->email)->send(new RegisterTokenMail($register_token, $this->createLink( $register_token )) );
    $send = new WCMail;
    $send = $send->send( $request->email,  new RegisterTokenMail($register_token, $this->createLink( $register_token )) );

    if( $send == 'Success'){
      return redirect()->back()->with('onsuccess','Email Sent to '.$register_token->email.' with RegisterToken!');
    }else{
      return redirect()->back()->with('onsuccess','Created RegisterToken, Email Sent Failed! ('.$send.')');
    }
  }

  protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'email' => 'required|string',
            'remote_app_id' => 'required|integer',
            'register_token' => 'required|string',
        ];

        $message = [
            'email.required' => 'Email is missing.',
            'remote_app_id.required' => 'Application is missing.',
            'register_token.required' => 'Register Token is missing.',
        ];

        $validator = Validator::make( $request, $rules, $message );

        return $validator;
    }

    protected function createLink( $register_token ){
      $app_url = Application::find( $register_token->remote_app_id )->remote_url;
      $app_url = rtrim($app_url, '/');

      return $app_url.'/register/'.$register_token->email.'/'.$register_token->register_token;
    }
}
