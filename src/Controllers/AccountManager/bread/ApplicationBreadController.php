<?php

namespace Gatekeeper\Controllers\AccountManager\bread;

use App\Http\Controllers\CradleController;
use Cradle\modules\bread\foundation\breadTrait;
use Cradle\modules\bread\contracts\BreadInterface;
use Cradle; 

class ApplicationBreadController extends CradleController implements BreadInterface
{
    use breadTrait;    
    
    // MAIN CONTROLLER SETTINGS //
    /*
    Function: Filter by Role / Permission for current controller
    Format: 
        $this->middleware('can:permission_name')
        $this->middleware('role:role_name');
    Example:
        $this->middleware('role:admin');
    */
    public function __construct()
    {
        $this->middleware('role:manager');
    }

    /*
    Function: Setup Main Model File used in current controller
    Format: 
        return new \App\Model;
    Example:
        return new \App\User;
    */
    protected function model()
    {
        return new \Gatekeeper\Models\Application;
    }
    
    /*
    Function: Setup PATH to Shared Common View Page (Layout) to show BREAD table.
    Format: 'admin.pages.bread';   OR   'admin/pages/bread';
    */
    protected function commonPage() 
    {
       return 'GKView::manager.pages.bread.browse';         
    }

    // BREAD TABLE SETTINGS //
    /*
    Function: Settings for BREAD table
    Default:
        'title'         => 'Table: '.$item_capital ,
        'item'          => $item_capital,
        'action_add'    => true,
        'action_edit'   => true,
        'action_delete' => true,
        'action_read'   => true,
        'route_index'   => route('bread.'.$item.'.index'),
        'route_browse'  => route('bread.'.$item.'.browse'),
        'route_read'    => route('bread.'.$item.'.read'),
        'route_edit'    => route('bread.'.$item.'.edit'), 
        'route_add'     => route('bread.'.$item.'.add'),            
        'route_delete'  => route('bread.'.$item.'.delete'),  
    */
    protected function settings()
    {
        // $item_capital = class_basename($this->model());
        $item_capital = 'Application';
        $item_space = preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", $item_capital);

        $item_underscore = preg_replace('/(?<=\\w)(?=[A-Z])/',"_$1", $item_capital); 
        $item = strtolower($item_underscore);

        return [
            'title'         => $item_space,
            'item'          => $item_space,
            'action_add'    => false,
            'action_edit'   => true,
            'action_delete' => true,
            'action_read'   => true,
            'route_index'   => route('bread.'.$item.'.index'),
            'route_browse'  => route('bread.'.$item.'.browse'),
            'route_read'    => route('bread.'.$item.'.read'),
            'route_edit'    => route('bread.'.$item.'.edit'), 
            'route_add'     => route('bread.'.$item.'.add'),            
            'route_delete'  => route('bread.'.$item.'.delete'),
                 
        ];
    }

    /*
    Function: Setup excluded columns in this table to show on BREAD table
    ~Return Array of column names
    @Param:
        col_name      -> Column Name in DB for Main Model.
    Format: 
        return ['col_name','col_name2','col_name3'];
    */
    protected function excludeCols()
    {
        return ['updated_at','created_at','deleted_at'];
    }

    protected function excludeColsRead()
    {
        return $this->excludeCols();
    }

    /*
    Function: Setup extra columns to show on BREAD table (Must set relation in Main Model)
    @Param:
        BREAD_Col   -> Column Name to be show in BREAD Table.
        method      -> Relationship method name to related Sub Model in Current Main Model.
        DB_Col      -> Column Name of data in Database for Sub Model. This column Data is read and printed in BREAD table.
    Format: 
        return [
            'BREAD_Col' => ['method','DB_Col'],
        ];
        
    Remarks: method can be repeated.
    */
    protected function extraCols()
    {
        return [
        ];
    }

     /*
    Function: settings for relation table 
    @Param:
        method      -> Relationship method name to related Sub Model in Current Main Model.
        item        -> Item name for this data, to be use for title/ action.
        Relation Type -> Relation type (for now: relation_pivot, relation_one, relation_many)
    Format: 
        return [
            'method' => ['item','Relation Type'],
        ];
    */
    protected function relationSettings()
    {
        return [
        ];
    }

    /*
    Function: settings for relation parent table (belongsTo) 
    @Param:
        method_from_parent  -> Relationship method name from parent model
        method_to_parent    -> Relationship method name to parent model
    Format: 
        return [
            'method_from_parent' => 'method_to_parent',
        ];
    */
    protected function relationParentSettings()
    {
        return [
        ];
    }
    
    /*
    Function: settings for relation table Cols shown in Read Page
    @Param:
        method      -> Relationship method name to related Sub Model in Current Main Model.
        col_name    -> Children table column name to be shown
        ** Leave empty will show all columns
    Format: 
        return [
            'method' => ['col_name1','col_name2'],
        ];

        ** Leave empty will prioritize read from children controller
        ** Place method(key) and leave value empty prioritize read from current controller, show all columns.
        ** Place method(key) and col_name(value) prioritize read from current controller, show columns in col_name arrray.
    */

    protected function relationColsRead()
    {
        return [];
    }

    /*
    Function: Replace column data in BREAD table with dataset name.
    @Param: 
        key     -> Column name of BREAD table, must be same as the key in dataset table
    Format: 
        return ['key1', 'key2']; //arrays of keys
    */
    protected function datasetCols()
    {        
        return [];
    }

   
    /*   
    Function: Table columns used in search function in BREAD table, Leave empty will search all columns.
    @Param: 
        col_name     -> Column name in BREAD table
    Format: 
        $search_cols = ['col_name1', 'col_name2']; // arrays of column name
    */
    protected function searchableCols()
    {
        $search_cols = [];
       
        if (empty($search_cols)) return $this->cols('read');
        
        return $search_cols;
    }

    // BREAD INPUT SETTINGS //
    /*   
    Function: Excluded columns that will not show in action modal input field.
    @Param: 
        col_name     -> Column name in BREAD table
    Format: 
        return ['col_name1', 'col_name2']; // arrays of column name
    */
    protected function excludeInput()
    {
        return [];
    }

    /*
    Function: Input field properties setup.
    @Param: 
        col_name     ->  Column name / input field in action modal
    Format: 
        return [
            'col_name1' => ['properties1', 'properties2'],
            'col_name2' => ['properties1', 'properties2'],
        ];
    
    Properties list available: 
    required => required input field to fill in, show * on label
    locked => locked input field, cannot input
    */
    protected function propInput()
    {
        return [
            'id' => ['locked'],
            'updated_at' => ['locked'],
            'created_at' => ['locked'],
            'deleted_at' => ['locked'],
        ];
    }

    /*
    Function: Input field type setup.
    @Param: 
        col_name    -> Column name / input field in action modal
        parameter_1 -> 'relation' - input for related Model (will get list of related Model Collection)
                    -> 'dataset' - input for dataset (will get list of dataset)
        parameter_2 -> 'method' - relation method to access the related Model 
                    -> 'key' - dataset key to get datalist for that key group
    Format: 
        return [
            'col_name1' => ['type'],
            'col_name2' => ['type', 'parameter_1', 'parameter_2', 'col_name_for_value', 'col_name_for_display_name'],
        ];

    Input Type list available: 
    - text (default)
    - textarea 
    - email 
    - number
    - password
    - dropdown - ** dropdown list (select 1)
    - multiple - ** dropdown list (select multiple) 
    **required: parameter_1, parameter 2 | optional: col_name_for_value, col_name_for_display_name
    */
    protected function typeInput()
    {
        return [
          'public' => ['boolean'],
          'pending' => ['boolean'],
          'blocked' => ['boolean'],  
        ];
    }   

    // BREAD SUBMIT SETTINGS //
    /*   
    Function: Columns to be validate when submit modal_edit. Laravel built in
    @Param: 
        col_name     -> Column name in input modal 
    Format: 
        return [
            'col_name1' => 'required|max:30',
            'col_name2' => 'required|email|max:255|unique:users',
        ];
    */
    protected function validateAddRules()
    {   
        return [];
    }

    /*   
    Function: Columns to be validate when submit modal_add. Laravel built in
    @Param: 
        col_name     -> Column name in input modal 
    Format: 
        return [
            'col_name1' => 'required|max:30',
            'col_name2' => 'required|email|max:255|unique:users',
        ];
    */
    protected function validateEditRules()
    {   
        return [];
    }

    /*   
    Function: Exclude columns to be saved into database
    @Param: 
        col_name     -> Column name in input modal 
    Format: 
         return ['col_name1', 'col_name2']; // arrays of column name
    */
    protected function excludeSave() {
        return ['updated_at','created_at','deleted_at'];
    }

    /*   
    Function: Set default value for the columns to be saved into database
    @Param: 
        col_name     -> Column name in input modal
        default_value -> default value for the columns
    Format: 
        return [
            'col_name1' => 'default_value',
            'col_name2' => 'default_value',
        ];
    */
    protected function defaultValue() {
        return [];
    }
}