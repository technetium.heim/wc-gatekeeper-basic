<?php

namespace Gatekeeper\Controllers\AccountManager;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;
use App\Models\User;
use Gatekeeper\Models\Application;
use Gatekeeper\Controllers\InputDataGeneratorTrait;

use Validator;
use Session;

class ApplicationController extends CradleController
{
	use InputDataGeneratorTrait;
 	protected $page;
	protected $redirectTo = '/';

	public function index()
	{
		// prepare role list 
		$user = new User;
		$roles = $user->roles()->getModel()->get();
		// (remove Admin and Manager)
		
		$roles = $this->generateDropdown( $roles, 'name', 'name' );

	    $this->page['title'] = ucwords(config('app.name'));
	    $this->page['meta']['description'] = ucwords(config('app.name'));
	    $this->page['roles'] = (array)$roles;
	    return view('GKView::manager/pages/application/new',$this->page);
	}

	public function linkApp(Request $request){
		// Set initial data for pending and blocked
		$request->request->add(['pending' => 0, 'blocked' => 0]);

		$request_param = $request->all();

		// validate all inputs
		$validator = $this->validateInputs($request_param);
        if ( $validator->fails() ) {
            return redirect()->back()
	            ->withInput($request->all())
	            ->withErrors($validator->errors());
        }

        // Save data
        $remote_app = new Application;
        $remote_app->app_name = $request->app_name;
        $remote_app->remote_url = $request->remote_url;
        $remote_app->role_key = $request->role_key;
        $remote_app->public = $request->public;
        $remote_app->pending = $request->pending;
        $remote_app->blocked = $request->blocked;
        $remote_app->save();

        return redirect()->back()->with('onsuccess','Application linked!');
	}


	protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'app_name' => 'required|string',
            'remote_url' => 'required|string',
            'role_key' => 'required|string',
            'public' => 'required|boolean',
            'blocked' => 'required|boolean',
        ];

        $message = [
            'app_name.required' => 'App Name is missing.',
            'remote_url.required' => 'Remote URL is missing.',
            'role_key.required' => 'Role Filter is missing.',
            'public.required' => 'Public Status is missing.',
            'pending.required' => 'Pending Status is missing.',
            'blocked.required' => 'Blocked Status is missing.',
        ];

        $validator = Validator::make( $request, $rules, $message );

        return $validator;
    }

    public function roleIndex()
    {
        $this->page['title'] = ucwords(config('app.name'));
        $this->page['meta']['description'] = ucwords(config('app.name'));
        return view('GKView::manager/pages/application/role',$this->page);
    }

    public function createRole(Request $request){
        $request_param = $request->all();

        // validate all inputs
        $validator = $this->validateInputsRole($request_param);
        if ( $validator->fails() ) {
            return redirect()->back()
                ->withInput($request->all())
                ->withErrors($validator->errors());
        }

        $user = new User;
        $role = $user->roles()->getModel();
        
        // Save data
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();

        return redirect()->back()->with('onsuccess','Role "'.$role->name.'"created!');
    }

    protected function validateInputsRole( $request ){
        // Validation 
        $rules = [
            'name' => 'required|string',
            'display_name' => 'required|string',
        ];

        $message = [
            'name.required' => 'Role Name is missing.',
            'display_name.required' => 'Role Display Name is missing.',
        ];

        $validator = Validator::make( $request, $rules, $message );

        return $validator;
    }

}
