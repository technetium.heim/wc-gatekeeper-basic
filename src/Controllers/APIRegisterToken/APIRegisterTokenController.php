<?php

namespace Gatekeeper\Controllers\APIRegisterToken;

use App\Http\Controllers\CradleController;
use Illuminate\Http\Request;

use Validator;
use Gatekeeper\Controllers\APIResponseTrait;
use Gatekeeper\Models\RegisterToken;
use Carbon\Carbon;
use Gatekeeper\Mail\RegisterTokenMail;
use Gatekeeper\Models\Application;
use App\Models\User;
use Hash;
use Cradle\tools\WCMail;
/*
  
*/
class APIRegisterTokenController extends CradleController
{
    use APIResponseTrait;

    public function browseRegisterTokenByApp(Request $request){
        $request = $this->processAppId($request);
        $request_param = $request->all();

        if( $request->app_name && !$request->app_id ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", $request->app_name.' Application Not Found') );
            return response()->json( $response );
        }
        
        // validate all inputs
        $validator = $this->validateInputs($request_param);
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }
            
        $result = RegisterToken::where('remote_app_id', $request->app_id)->get();

        $response = $this->APIResponse( $request_param, $this->getResp("S"), $result );
        return response()->json($response);
    }


    // Create Register Token And Send Email To User
    // public function sendRegisterToken(Request $request){
    //     $request = $this->processAppId($request);
    //     $request_param = $request->all();

    //     if( $request->app_name && !$request->app_id ){
    //         $response = $this->APIResponse( $request_param, $this->getResp("F", $request->app_name.' Application Not Found') );
    //         return response()->json( $response );
    //     }

    //     $validator = Validator::make( $request_param, ['app_id' => 'required', 'email' => 'required'], [ 'app_id.required' => 'App Id/ App Name is missing.', 'email.required' => 'Email is missing.' ]);
    //     if ( $validator->fails() ) {
    //         $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
    //         return response()->json( $response );
    //     }

    //     $register_token =  $this->createNewToken($request);
    
    //     // return redirect()->back()->with('onsuccess','Register Token Created! (send email is not working)');
    //     // check for email settings
    //     if( $request->send_email ){
    //         Mail::to($request->email)->send(new RegisterTokenMail($register_token) );
    //         $email_sent = 'Yes';
    //     }else{
    //         $email_sent = 'No';
    //     }


    //     $response = $this->APIResponse( $request_param, $this->getResp("S", 'Email Sent to '.$register_token->email.' with RegisterToken!') );
    //     return response()->json($response);
    // }

    // Create Register Token And Account + Send Email To User
    public function sendWithAccRegisterToken(Request $request){

        $request = $this->processAppId($request);
        $request_param = $request->all();

        if( $request->app_name && !$request->app_id ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", $request->app_name.' Application Not Found') );
            return response()->json( $response );
        }
        
        $validator = Validator::make( $request_param, ['app_id' => 'required', 'email' => 'required'], [ 'app_id.required' => 'App Id/ App Name is missing.', 'email.required' => 'Email is missing.', ]);
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        // create user account 
        $user = User::where('email', $request->email)->first();
        
        $is_create_remote_account = false;

        if(!$user){
            $user = new User;
            $user->email = $request->email;
            $user->name = "DefaultUser";
            $user->password = Hash::make('password');
            $user->save();
            
            $is_create_remote_account = true;
                
        }else{

            $remote_account = $user->remote_accounts()->where('remote_app_id', $request->app_id )->first();

            // Addon: To block repeat send token for the same remote account, use resendRegisterTokenEmail instead
            if($remote_account){
                if( $remote_account->isActivated ){
                    $response = $this->APIResponse( $request_param, $this->getResp("F", 'Email already Registered. Please use another email.' ) );
                }else{
                    $response = $this->APIResponse( $request_param, $this->getResp("F", 'Email already in registration process. Please use Resend Token instead.' ) );
                }
                return response()->json( $response );
            }
            if(!$remote_account){
                $is_create_remote_account = true;
            }
        }

        if($is_create_remote_account){
            $session_expired_date = Carbon::now()->addSeconds( config('AccServer.remote_session.lifetime') )->toDateTimeString();

            $remote_account = $user->remote_accounts()->getModel();
            $remote_account->user_id = $user->id;
            $remote_account->remote_app_id = $request->app_id;
            $remote_account->remote_account_token = $this->generateCode(20);
            $remote_account->remote_session_token = $this->generateCode(40);
            $remote_account->expired_at = $session_expired_date;
            $remote_account->save();
        }

        $request->request->add(['remote_account_id' => $remote_account->id ]);

        $register_token =  $this->createNewToken($request);
            
        // return redirect()->back()->with('onsuccess','Register Token Created! (send email is not working)');
        // check for email settings
        if($request->send_email == "" ){
            $request->send_email = "Yes";
        }

        if( $request->send_email == "Yes"){
            $send = new WCMail;
            $send = $send->send( $request->email,  new RegisterTokenMail($register_token, $this->createLink( $register_token )) );

            if( $send == 'Success'){
                $email_sent = 'Yes';
                $msg = 'Email Sent to '.$register_token->email.' with RegisterToken!';
            }else{
                $email_sent = 'No';
                $msg = 'Created RegisterToken, Email Sent Failed! ('.$send.')';
            }
        }else{
            $email_sent = 'No';
            $msg = 'Created RegisterToken, Email Not Sent!';
        }

        $result = (object)[
            'remote_account_user_id'=> $user->id,
            'remote_account_id' => $remote_account->id,
            'email' => $request->email,
            'email_sent' =>  $email_sent,
            'link' => $this->createLink( $register_token ),
            'register_token' => $register_token->register_token,
        ];

        $response = $this->APIResponse( $request_param, $this->getResp("S", $msg ) , $result );
        return response()->json($response);
    }

    // Resend Registration Token
    public function resendRegisterTokenEmail(Request $request){
        $request = $this->processAppId($request);
        $request_param = $request->all();

        if( $request->app_name && !$request->app_id ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", $request->app_name.' Application Not Found') );
            return response()->json( $response );
        }
        
        $validator = Validator::make( $request_param, ['app_id' => 'required', 'email' => 'required'], [ 'app_id.required' => 'App Id/ App Name is missing.', 'email.required' => 'Email is missing.', ]);
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        // create user account 
        $user = User::where('email', $request->email)->first();
        
        if(!$user){
           $response = $this->APIResponse( $request_param, $this->getResp("F", $request->app_name.' Registering Account Not Found') );
            return response()->json( $response ); 
        }

        $remote_account = $user->remote_accounts()->where('remote_app_id', $request->app_id )->first();

        if(!$remote_account){
           $response = $this->APIResponse( $request_param, $this->getResp("F", $request->app_name.' Registering Account Not Found') );
            return response()->json( $response ); 
        }

        if( $remote_account->isActivated ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", 'Resend Register Token not allowed. Account registration already completed.' ) );
            return response()->json( $response ); 
        }

        $register_token =  $this->createNewToken($request);

        if($request->send_email == "" ){
            $request->send_email = "Yes";
        }

        if( $request->send_email == "Yes"){
            $send = new WCMail;
            $send = $send->send( $request->email,  new RegisterTokenMail($register_token, $this->createLink( $register_token )) );

            if( $send == 'Success'){
                $email_sent = 'Yes';
                $msg = 'Email Sent to '.$register_token->email.' with RegisterToken!';
            }else{
                $email_sent = 'No';
                $msg = 'Created RegisterToken, Email Sent Failed! ('.$send.')';
            }
        }else{
            $email_sent = 'No';
            $msg = 'Created RegisterToken, Email Not Sent!';
        }

        $result = (object)[
            'remote_account_user_id'=> $user->id,
            'remote_account_id' => $remote_account->id,
            'email' => $request->email,
            'email_sent' =>  $email_sent,
            'link' => $this->createLink( $register_token ),
            'register_token' => $register_token->register_token,
        ];

        $response = $this->APIResponse( $request_param, $this->getResp("S", $msg ) , $result );
        return response()->json($response);

    }

    // Create Register Token and Return Register Link (email + token in url)
    public function createRegisterLink(Request $request){
        $request = $this->processAppId($request);
        $request_param = $request->all();

        if( $request->app_name && !$request->app_id ){
            $response = $this->APIResponse( $request_param, $this->getResp("F", $request->app_name.' Application Not Found') );
            return response()->json( $response );
        }
        
        $validator = Validator::make( $request_param, ['app_id' => 'required', 'email' => 'required'], [ 'app_id.required' => 'App Id/ App Name is missing.', 'email.required' => 'Email is missing.', ]);
        if ( $validator->fails() ) {
            $response = $this->APIResponse( $request_param, $this->getResp("F", $validator->errors()->first() ) );
            return response()->json( $response );
        }

        $register_token =  $this->createNewToken($request);

        $result = (object)[
          'register_link' => $this->createLink( $register_token ),
        ];

        $response = $this->APIResponse( $request_param, $this->getResp("S", 'Token Created.' ), $result);
        return response()->json($response);
    }

    protected function createLink( $register_token ){
      $app_url = Application::find( $register_token->remote_app_id )->remote_url;
      $app_url = rtrim($app_url, '/');

      return $app_url.'/register/'.$register_token->email.'/'.$register_token->register_token;
    }

    protected function createNewToken($request){
        $request->request->add(['register_token' => $this->generateCode( 8) ]);
      
        $expired_date = Carbon::now()->addSeconds( config('gatekeeper.remote_session.lifetime') )->toDateTimeString();
  
        $register_token = RegisterToken::where('email', $request->email )->where('remote_app_id', $request->app_id )->first();
        

        if ($register_token){
          // replace existing data
          $register_token->register_token = $request->register_token;

          if( $request->remote_account_id ){
            $register_token->remote_account_id = $request->remote_account_id;
          }

          $register_token->expired_at = $expired_date;
          $register_token->save();
        }else{
          // Save new data
          $register_token = new RegisterToken;
          $register_token->email = $request->email;
          $register_token->remote_app_id = $request->app_id;
        
          if( $request->remote_account_id ){
            $register_token->remote_account_id = $request->remote_account_id;
          }

          $register_token->register_token = $request->register_token;
          $register_token->expired_at = $expired_date;
          $register_token->save();
        }
 
        return $register_token;
    }

    protected function processAppId( $request ){
      if( !$request->app_id && $request->app_name){

        $app = Application::where('app_name', $request->app_name )->first();
        
        if ( $app ) {
            $request->request->add(['app_id' => $app->id ]);
        }
      }

      return $request;
    }

    protected function validateInputs( $request ){
        // Validation 
        $rules = [
            'app_id' => 'required',
        ];

        $message = [
            'app_id.required' => 'App Id/ App Name is missing.',
        ];

        $validator = Validator::make( $request, $rules, $message );

        return $validator;
    }
}
