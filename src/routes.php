<?php 

Route::group(['middleware' => 'web'], function() { // middleware web required for error share
// ::::: ACCOUNT MANAGER ROUTE:::::://

	
	// Account landing page
	Route::group(['middleware' => [ 'gatekeeper:out'] ], function () {
		Route::get('/', 'Gatekeeper\Controllers\AccountManager\auth\AuthController@index')->name('account.manager.landing');
		Route::post('/login', 'Gatekeeper\Controllers\AccountManager\auth\AuthController@login')->name('account.manager.login');
	});
	
	Route::post('/logout', 'Gatekeeper\Controllers\AccountManager\auth\AuthController@logout')->name('account.manager.logout');

	//-------------------------------------------

	Route::group(['middleware' => [ 'gatekeeper:in'] ], function () {
		Route::get('/restrict',  'Gatekeeper\Controllers\AccountManager\auth\AuthController@restrict')->name('account.manager.restrict');	
	});

	//-------------------------------------------

	// Filtered Account Page (Dashboard) -> after login
	Route::group(['middleware' => [ 'gatekeeper:in' ] ], function () {
		// Dashboard
		Route::get('/dashboard', 'Gatekeeper\Controllers\AccountManager\auth\AuthController@dashboard')->name('account.manager.dashboard');
		
		// Application
		Route::get('/application/new/index', 'Gatekeeper\Controllers\AccountManager\ApplicationController@index')->name('account.manager.application.new.index');
		Route::post('/application/new', 'Gatekeeper\Controllers\AccountManager\ApplicationController@linkApp')->name('account.manager.application.new');
		Route::get('/application/role/index', 'Gatekeeper\Controllers\AccountManager\ApplicationController@roleIndex')->name('account.manager.application.role.index');
		Route::post('/application/role/create', 'Gatekeeper\Controllers\AccountManager\ApplicationController@createRole')->name('account.manager.application.role.create');
		Cradle::routesBread('application', 'Gatekeeper\Controllers\AccountManager\bread\ApplicationBreadController');

		// Account or User
		Route::get('/account/browse', 'Gatekeeper\Controllers\AccountManager\AccountController@browse')->name('account.manager.account.browse');
		Cradle::routesBread('user', 'Gatekeeper\Controllers\AccountManager\bread\UserBreadController', null, 'manager');
		Cradle::routesBread('role', 'Gatekeeper\Controllers\AccountManager\bread\RoleBreadController', null, 'manager');
		
		// Register token
		Route::get('/register_token/create/index', 'Gatekeeper\Controllers\AccountManager\RegisterTokenController@index')->name('account.manager.token.create.index');
		Route::post('/register_token/send', 'Gatekeeper\Controllers\AccountManager\RegisterTokenController@sendToken')->name('account.manager.token.send');
		Cradle::routesBread('register_token', 'Gatekeeper\Controllers\AccountManager\bread\RegisterTokenBreadController');
		
	});

// END ACCOUNT MANAGER ROUTE

});