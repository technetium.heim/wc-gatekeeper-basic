<?php

namespace Gatekeeper\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Keymaster\foundation\RAUserGroupTrait;

class GatekeeperAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $status)
    {
        if($status == 'in'){
            // if not login will redirect
            if( !Auth::check() ){
                return redirect()->route( config('gatekeeper.unauthenticated_redirect') );
            }
            if( Auth::check() && !Auth::user()->hasRole('manager') ){
                return redirect()->route( config('gatekeeper.unauthenticated_redirect') );
            }
        }

        if($status == 'out'){
            // if logged in will redirect

            if( Auth::check() && Auth::user()->hasRole('manager') ){
                return redirect()->route( config('gatekeeper.authenticated_redirect') );
            }
        }

        return $next($request);
    }
}
