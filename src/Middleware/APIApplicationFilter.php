<?php

namespace Gatekeeper\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Gatekeeper\Models\Application;
use Gatekeeper\Controllers\APIResponseTrait;

class APIApplicationFilter
{
    use APIResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return response()->json( $request );
        // check application
        $check_app = Application::where('app_name', $request->connect_app_name )
                                ->where('role_key', $request->connect_role_key )
                                ->first();

        if( !$check_app ){
            $response = $this->APIResponse( $request->all(), $this->getResp("F", "Unauthenticated. Code E01." ,"Application Not Linked.") );
            return response()->json( $response );   
        }

        // verify host url
        if( preg_replace("(^https?://)", "", $check_app->remote_url ) != preg_replace("(^https?://)", "", $request->connect_remote_url ) ){
            $response = $this->APIResponse( $request->all(), $this->getResp("F", "Unauthenticated. Code E02." ,"Application URL not match.") );
            return response()->json( $response );   
        }

        // check pending status
        if( $check_app->pending ){
            $response = $this->APIResponse( $request->all(), $this->getResp("F", "Unauthenticated. Code E03." ,"Application linking is Pending.") );
            return response()->json( $response );   
        }

        // check blocked status
        if( $check_app->blocked ){
            $response = $this->APIResponse( $request->all(), $this->getResp("F", "Unauthenticated. Code E04." ,"Application is blocked.") );
            return response()->json( $response );   
        }

        // add the remote_app_id in
        $request->request->add(['connected_app_id' => $check_app->id ]);
        $request->request->add(['connected_public' =>  $check_app->public ]);
        $request->request->add(['connected_role_key' =>  $check_app->role_key ]);
        return $next($request);
    }
}
