<?php

namespace Gatekeeper\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Keymaster\foundation\RAUserGroupTrait;
use Gatekeeper\Controllers\APIResponseTrait;

class GateAdmin
{
    use APIResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( !$request->gate_token ){
            $response = $this->APIResponse( $request->all(), $this->getResp("F", "Required admin permission. Missing Gate Token." ) );
            return response()->json( $response );   
        }

        if( $request->gate_token != env('GATE_TOKEN', 'GATE_TOKEN') ){
            $response = $this->APIResponse( $request->all(), $this->getResp("F", "Required admin permission. Mismatched Gate Token.") );
            return response()->json( $response ); 
        }

        return $next($request);
    }
}
